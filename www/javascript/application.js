class Category {
    #categoryId;
    #categoryName;
    #pet;
    #formErrors = 0;

    constructor(id, name) {
        this.#categoryId = id;
        this.#categoryName = name;
    }

    setPet(pet) {
        this.#pet = pet;
    }

    render() {
        let divEl = jQuery('<div>');
        divEl.addClass('pet__category');
        divEl.attr('id', 'pet-' + this.#pet.petId + '-cat-' + this.#categoryId);
        divEl.text(this.#categoryName);

        return divEl;
    }

    renderForm(newRecord = true) {
        let self = this;

        let categoryFieldSetEl = jQuery('<fieldset>');
        categoryFieldSetEl.addClass('category');

        let categoryFieldSetLegendEl = jQuery('<legend>');
        categoryFieldSetLegendEl.text('Category');
        categoryFieldSetEl.append(categoryFieldSetLegendEl);

        if (newRecord) {
            let idFieldWrapEl = jQuery('<div>');
            idFieldWrapEl.addClass('field');
            categoryFieldSetEl.append(idFieldWrapEl);

            let idFieldLabelEl = jQuery('<label>');
            idFieldLabelEl.text('ID:');
            idFieldWrapEl.append(idFieldLabelEl);

            let idFieldInputEl = jQuery('<input>');
            idFieldInputEl.addClass('field__input');
            idFieldInputEl.attr('type', 'number');
            idFieldInputEl.attr('name', 'category-id');
            idFieldInputEl.attr('min', 1);
            idFieldInputEl.attr('value', this.#categoryId ? this.#categoryId : '');
            idFieldWrapEl.append(idFieldInputEl);
            idFieldInputEl.on('change blur keyup', function() {
                let value = jQuery(this).val();
                if (value.match(/^\d+$/) && value >= 1) {
                    if (jQuery(this).hasClass('error')) {
                        jQuery(this).removeClass('error');
                        self.#formErrors--;
                    }
                    self.#categoryId = value;
                } else {
                    if (!jQuery(this).hasClass('error')) {
                        self.#formErrors++;
                        jQuery(this).addClass('error');
                    }
                }
            });
        }

        let nameFieldWrapEl = jQuery('<div>');
        nameFieldWrapEl.addClass('field');
        categoryFieldSetEl.append(nameFieldWrapEl);

        let nameFieldLabelEl = jQuery('<label>');
        nameFieldLabelEl.text('Name:');
        nameFieldWrapEl.append(nameFieldLabelEl);

        let nameFieldInputEl = jQuery('<input>');
        nameFieldInputEl.addClass('field__input');
        nameFieldInputEl.attr('type', 'text');
        nameFieldInputEl.attr('name', 'category-name');
        nameFieldInputEl.attr('value', this.#categoryName);
        nameFieldWrapEl.append(nameFieldInputEl);
        nameFieldInputEl.on('change blur keyup', function () {
            let value = jQuery(this).val();
            if (value.trim() !== '') {
                if (jQuery(this).hasClass('error')) {
                    jQuery(this).removeClass('error');
                    self.#formErrors--;
                }
                self.#categoryName = value.trim();
            } else {
                if (!jQuery(this).hasClass('error')) {
                    self.#formErrors++;
                    jQuery(this).addClass('error');
                }
            }
        });

        return categoryFieldSetEl;
    }

    hasFormErrors() {
        return this.#formErrors > 0;
    }

    get json() {
        return {
            'id': parseInt(this.#categoryId),
            'name': this.#categoryName,
        };
    }
}

class Tag {
    #tagId;
    #tagName;
    #pet;
    #formErrors;

    constructor(id, name) {
        this.#tagId = id;
        this.#tagName = name;
    }

    setPet(pet) {
        this.#pet = pet;
    }

    render() {
        let tagEl = jQuery('<span>');
        tagEl.addClass('pet__tag');
        tagEl.attr('id', 'pet-' + this.#pet.petId + '-tag-' + this.#tagId);
        tagEl.addClass('pet__tag--color-' + (Math.abs(this.tagHash) % 10));
        tagEl.text(this.#tagName);

        return tagEl;
    }

    renderForm() {
        let self = this;

        let mainDivEl = jQuery('<div>');
        mainDivEl.addClass('tag');

        let idFieldWrapEl = jQuery('<div>');
        idFieldWrapEl.addClass('field');
        mainDivEl.append(idFieldWrapEl);

        let idFieldLabelEl = jQuery('<label>');
        idFieldLabelEl.text('ID:');
        idFieldWrapEl.append(idFieldLabelEl);

        let idFieldInputEl = jQuery('<input>');
        idFieldInputEl.addClass('field__input');
        idFieldInputEl.attr('type', 'number');
        idFieldInputEl.attr('name', 'tag-id');
        idFieldInputEl.attr('min', 1);
        idFieldInputEl.attr('value', this.#tagId ? this.#tagId : '');
        idFieldWrapEl.append(idFieldInputEl);
        idFieldInputEl.on('change blur keyup', function() {
            let value = jQuery(this).val();
            let index = mainDivEl.index();

            if (value.match(/^\d+$/) && value >= 1 && self.#pet.validateTagsIdNotContain(value, index)) {
                if (jQuery(this).hasClass('error')) {
                    jQuery(this).removeClass('error');
                    self.#formErrors--;
                }
                self.#tagId = value;
            } else {
                if (!jQuery(this).hasClass('error')) {
                    self.#formErrors++;
                    jQuery(this).addClass('error');
                }
            }
        });

        let nameFieldWrapEl = jQuery('<div>');
        nameFieldWrapEl.addClass('field');
        mainDivEl.append(nameFieldWrapEl);

        let nameFieldLabelEl = jQuery('<label>');
        nameFieldLabelEl.text('Name:');
        nameFieldWrapEl.append(nameFieldLabelEl);

        let nameFieldInputEl = jQuery('<input>');
        nameFieldInputEl.addClass('field__input');
        nameFieldInputEl.attr('type', 'text');
        nameFieldInputEl.attr('name', 'tag-name');
        nameFieldInputEl.attr('value', this.#tagName);
        nameFieldWrapEl.append(nameFieldInputEl);
        nameFieldInputEl.on('change blur keyup', function () {
            let value = jQuery(this).val();
            if (value.match(/^[a-zA-Z0-9]+$/)) {
                if (jQuery(this).hasClass('error')) {
                    jQuery(this).removeClass('error');
                    self.#formErrors--;
                }
                self.#tagName = value;
            } else {
                if (!jQuery(this).hasClass('error')) {
                    self.#formErrors++;
                    jQuery(this).addClass('error');
                }
            }
        });

        let deleteTagWrapEl = jQuery('<div>');
        deleteTagWrapEl.addClass('delete');
        mainDivEl.append(deleteTagWrapEl);

        let deleteButtonEl = jQuery('<button>');
        deleteButtonEl.addClass('button');
        deleteButtonEl.addClass('button--danger');
        deleteButtonEl.text('Delete this tag');
        deleteTagWrapEl.append(deleteButtonEl);

        deleteButtonEl.on('click', function () {
            if (confirm('Are you sure about deletion of this tag?')) {
                self.#pet.removeTag(mainDivEl.index(), mainDivEl);
            }
        });

        return mainDivEl;
    }

    hasFormErrors() {
        return this.#formErrors > 0;
    }

    getId() {
        return this.#tagId;
    }

    get tagHash() {
        let hash = 0;

        if (this.#tagName.length === 0) {
            return hash;
        }

        for (let i = 0; i < this.#tagName.length; ++i) {
            let char = this.#tagName.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash |= 0;
        }

        return hash;
    }

    get json() {
        return {
            'id': parseInt(this.#tagId),
            'name': this.#tagName,
        };
    }
}

class Picture {
    #url;
    #pet;
    #formErrors;

    constructor(url) {
        this.#url = url;
    }

    setPet(pet) {
        this.#pet = pet;
    }

    render() {
        let self = this;
        let imgEl = jQuery('<img>');
        imgEl.addClass('pet__picture');
        imgEl.attr('src', this.#url);
        imgEl.on('click', function () {
            window.open(self.#url);
        });

        return imgEl;
    }

    renderForm() {
        let self = this;

        let mainEl = jQuery('<div>');
        mainEl.addClass('picture');

        let urlFieldWrapEl = jQuery('<div>');
        urlFieldWrapEl.addClass('field');
        mainEl.append(urlFieldWrapEl);

        let urlFieldLabelEl = jQuery('<label>');
        urlFieldLabelEl.text('Image:');
        urlFieldWrapEl.append(urlFieldLabelEl);

        let urlFieldImageEl = jQuery('<img>');
        urlFieldImageEl.attr('src', self.#url);
        urlFieldImageEl.addClass('field__image');
        urlFieldImageEl.addClass('field__as-link');
        urlFieldWrapEl.append(urlFieldImageEl);
        urlFieldImageEl.on('click', function () {
            window.open(self.#url);
        });

        let deleteTagWrapEl = jQuery('<div>');
        deleteTagWrapEl.addClass('delete');
        mainEl.append(deleteTagWrapEl);

        let deleteButtonEl = jQuery('<button>');
        deleteButtonEl.addClass('button');
        deleteButtonEl.addClass('button--danger');
        deleteButtonEl.text('Delete this picture');
        deleteTagWrapEl.append(deleteButtonEl);

        deleteButtonEl.on('click', function () {
            if (confirm('Are you sure about deletion of this picture?')) {
                self.#pet.removePicture(mainEl.index(), mainEl);
            }
        });

        return mainEl;
    }

    hasFormErrors() {
        return this.#formErrors > 0;
    }

    get json() {
        return this.#url;
    }
}

class Pet {
    #petId;
    #petName;
    #category;
    #tags;
    #pictures
    #status;
    #formErrors = 0;
    #application;

    constructor(id, name, category, tags = [], pictures = [], status = 'available') {
        this.#petId = id;
        this.#petName = name;
        this.#category = category;
        this.#tags = tags;
        this.#pictures = pictures;
        this.#status = status;

        this.#category.setPet(this);

        for (let t in this.#tags) {
            this.#tags[t].setPet(this);
        }

        for (let p in this.#pictures) {
            this.#pictures[p].setPet(this);
        }
    }

    setApplication(app) {
        this.#application = app;
    }

    get petId() {
        return this.#petId;
    }

    get json() {
        let tags = [];
        let pictures = [];

        for (let t in this.#tags) {
            tags.push(this.#tags[t].json);
        }

        for (let p in this.#pictures) {
            pictures.push(this.#pictures[p].json);
        }

        return {
            'id': parseInt(this.#petId),
            'name': this.#petName,
            'status': this.#status,
            'category': this.#category.json,
            'tags': tags,
            'photoUrls': pictures,
        }
    }

    removeTag(index, formElementToDelete = null) {
        if (typeof this.#tags[index] === 'undefined') {
            return;
        }

        this.#tags.splice(index, 1);

        if (formElementToDelete) {
            formElementToDelete.remove();
        }
    }

    removePicture(index, formElementToDelete = null) {
        if (typeof this.#pictures[index] === 'undefined') {
            return;
        }

        this.#pictures.splice(index, 1);

        if (formElementToDelete) {
            formElementToDelete.remove();
        }
    }

    render(renderMenu = true) {
        let petEl = jQuery('<div>');
        petEl.addClass('pet');
        petEl.addClass('pet--' + this.#status);
        petEl.attr('id', 'pet-' + this.#petId);

        let nameEl = jQuery('<div>');
        nameEl.addClass('pet__name');
        nameEl.text(this.#petName);
        petEl.append(nameEl);

        if (renderMenu) {
            nameEl.append(this.#renderPetMenu());
        }

        petEl.append(this.#category.render());

        let picturesEl = jQuery('<div>');
        picturesEl.addClass('pet__pictures');
        petEl.append(picturesEl);

        for (let p in this.#pictures) {
            picturesEl.append(this.#pictures[p].render());
        }

        let tagsEl = jQuery('<div>');
        tagsEl.addClass('pet__tags');
        petEl.append(tagsEl);

        for (let tag in this.#tags) {
            tagsEl.append(this.#tags[tag].render(this.#petId));
        }

        let statusEl = jQuery('<div>');
        statusEl.addClass('pet__status');
        statusEl.text(this.#status);
        petEl.append(statusEl);

        return petEl;
    }

    #renderPetMenu() {
        let menuMainEl = jQuery('<div>');
        menuMainEl.addClass('pet__menu');

        let navEl = jQuery('<nav>');
        menuMainEl.append(navEl);

        let spanEl = jQuery('<span>');
        spanEl.text('...');
        navEl.append(spanEl);

        let wrapEl = jQuery('<div>');
        wrapEl.addClass('pet__menu__wrap');
        navEl.append(wrapEl);

        let editLinkEl = jQuery('<a>');
        editLinkEl.attr('href', '#editPet=' + this.#petId);
        editLinkEl.text('Edit');
        wrapEl.append(editLinkEl);

        let quickEditEl = jQuery('<a>');
        quickEditEl.attr('href', '#quickEditPet=' + this.#petId);
        quickEditEl.text('Quick update');
        wrapEl.append(quickEditEl);

        let uploadEl = jQuery('<a>');
        uploadEl.attr('href', '#uploadPicture=' + this.#petId);
        uploadEl.text('Upload picture');
        wrapEl.append(uploadEl);

        let deleteEl = jQuery('<a>');
        deleteEl.attr('href', '#deletePet=' + this.#petId);
        deleteEl.addClass('danger');
        deleteEl.text('Delete');
        wrapEl.append(deleteEl);

        return menuMainEl;
    }

    renderDeletePetForm() {
        let self = this;

        let petEl = jQuery('<div>');
        petEl.addClass('pet-form');

        let promptEl = jQuery('<p>');
        promptEl.addClass('prompt');
        promptEl.text('Are you sure about deletion of this pet with all related data?');
        petEl.append(promptEl);

        let controlPanelDivEl = jQuery('<div>');
        controlPanelDivEl.addClass('controls');
        petEl.append(controlPanelDivEl);

        let deleteButtonEl = jQuery('<button>');
        deleteButtonEl.addClass('button');
        deleteButtonEl.addClass('button--danger');
        deleteButtonEl.text('Delete pet');
        controlPanelDivEl.append(deleteButtonEl);
        deleteButtonEl.on('click', function () {
            self.#application.deletePet(self.#petId);
        });

        let cancelButtonEl = jQuery('<a>');
        cancelButtonEl.attr('href', '#main');
        cancelButtonEl.addClass('button');
        cancelButtonEl.text('Cancel');
        controlPanelDivEl.append(cancelButtonEl);

        return petEl;
    }

    renderUploadPictureForm() {
        let self = this;

        let petEl = jQuery('<div>');
        petEl.addClass('pet-form');

        let pictureFieldWrapEl = jQuery('<div>');
        pictureFieldWrapEl.addClass('field');
        petEl.append(pictureFieldWrapEl);

        let pictureFieldLabelEl = jQuery('<label>');
        pictureFieldLabelEl.text('Picture:');
        pictureFieldWrapEl.append(pictureFieldLabelEl);

        let pictureFieldInputEl = jQuery('<input>');
        pictureFieldInputEl.addClass('field__input');
        pictureFieldInputEl.attr('type', 'file');
        pictureFieldInputEl.attr('accept', 'image/gif, image/png, image/jpeg, image/webp');
        pictureFieldWrapEl.append(pictureFieldInputEl);
        pictureFieldInputEl.on('change', function () {
            let value = jQuery(this).val();

            if (value === '') {
                if (!jQuery(this).hasClass('error')) {
                    jQuery(this).addClass('error');
                    self.#formErrors++;
                }

                return;
            }

            let file = jQuery(this)[0].files[0];
            let type = file.type;

            if (type !== 'image/jpeg' && type !== 'image/png' && type !== 'image/gif' && type !== 'image/webp') {
                if (!jQuery(this).hasClass('error')) {
                    jQuery(this).addClass('error');
                    self.#formErrors++;
                }
            } else {
                if (jQuery(this).hasClass('error')) {
                    jQuery(this).removeClass('error');
                    self.#formErrors--;
                }
            }
        });

        let pictureHintEl = jQuery('<p>');
        pictureHintEl.addClass('field__hint');
        pictureHintEl.text('Here you can upload a picture to the pet. Select picture of type PNG, JPEG, GIF or WEBP.');
        petEl.append(pictureHintEl);

        let controlPanelDivEl = jQuery('<div>');
        controlPanelDivEl.addClass('controls');
        petEl.append(controlPanelDivEl);

        let saveButtonEl = jQuery('<button>');
        saveButtonEl.addClass('button');
        saveButtonEl.addClass('button--important');
        saveButtonEl.text('Upload picture');
        controlPanelDivEl.append(saveButtonEl);
        saveButtonEl.on('click', function () {
            jQuery('.pet-form .field__input').change();

            if (!self.hasFormErrors()) {
                let file = pictureFieldInputEl[0].files[0];
                let type = file.type;

                let reader = new FileReader();
                reader.onload = function () {
                    saveButtonEl.removeAttr('disabled');
                    self.#application.uploadPicture(self.#petId, new Uint8Array(reader.result), type);
                };
                saveButtonEl.attr('disabled', 'disabled');
                reader.readAsArrayBuffer(file);
            }
        });

        let cancelButtonEl = jQuery('<a>');
        cancelButtonEl.attr('href', '#main');
        cancelButtonEl.addClass('button');
        cancelButtonEl.text('Cancel');
        controlPanelDivEl.append(cancelButtonEl);

        return petEl;
    }

    renderQuickUpdateForm() {
        let self = this;
        let petEl = jQuery('<div>');
        petEl.addClass('pet-form');

        let nameFieldWrapEl = jQuery('<div>');
        nameFieldWrapEl.addClass('field');
        petEl.append(nameFieldWrapEl);

        let nameFieldLabelEl = jQuery('<label>');
        nameFieldLabelEl.text('Name:');
        nameFieldWrapEl.append(nameFieldLabelEl);

        let nameFieldInputEl = jQuery('<input>');
        nameFieldInputEl.addClass('field__input');
        nameFieldInputEl.attr('type', 'text');
        nameFieldInputEl.attr('name', 'pet-name');
        nameFieldInputEl.attr('value', this.#petName);
        nameFieldWrapEl.append(nameFieldInputEl);
        nameFieldInputEl.on('change blur keyup', function () {
            let value = jQuery(this).val();
            if (value.trim() !== '') {
                if (jQuery(this).hasClass('error')) {
                    jQuery(this).removeClass('error');
                    self.#formErrors--;
                }
            } else {
                if (!jQuery(this).hasClass('error')) {
                    self.#formErrors++;
                    jQuery(this).addClass('error');
                }
            }
        });

        let statusFieldWrapEl = jQuery('<div>');
        statusFieldWrapEl.addClass('field');
        petEl.append(statusFieldWrapEl);

        let statusFieldLabelEl = jQuery('<label>');
        statusFieldLabelEl.text('Status:');
        statusFieldWrapEl.append(statusFieldLabelEl);

        let statusFieldSelectEl = jQuery('<select>');
        statusFieldSelectEl.addClass('field__input');
        statusFieldSelectEl.attr('size', 1);
        statusFieldSelectEl.attr('name', 'pet-status');
        statusFieldWrapEl.append(statusFieldSelectEl);

        let statusOptionAvailableEl = jQuery('<option>');
        statusOptionAvailableEl.attr('value', 'available');
        if (this.#status === 'available') {
            statusOptionAvailableEl.attr('selected', 'selected');
        }
        statusOptionAvailableEl.text('available');
        statusFieldSelectEl.append(statusOptionAvailableEl);

        let statusOptionPendingEl = jQuery('<option>');
        statusOptionPendingEl.attr('value', 'pending');
        if (this.#status === 'pending') {
            statusOptionPendingEl.attr('selected', 'selected');
        }
        statusOptionPendingEl.text('pending');
        statusFieldSelectEl.append(statusOptionPendingEl);

        let statusOptionSoldEl = jQuery('<option>');
        statusOptionSoldEl.attr('value', 'sold');
        if (this.#status === 'sold') {
            statusOptionSoldEl.attr('selected', 'selected');
        }
        statusOptionSoldEl.text('sold');
        statusFieldSelectEl.append(statusOptionSoldEl);

        let controlPanelDivEl = jQuery('<div>');
        controlPanelDivEl.addClass('controls');
        petEl.append(controlPanelDivEl);

        let saveButtonEl = jQuery('<button>');
        saveButtonEl.addClass('button');
        saveButtonEl.addClass('button--important');
        saveButtonEl.text('Update pet');
        controlPanelDivEl.append(saveButtonEl);
        saveButtonEl.on('click', function () {
            jQuery('.pet-form .field__input').change();

            if (!self.hasFormErrors()) {
                self.#application.quickUpdatePet(
                    self.#petId,
                    nameFieldInputEl.val(),
                    statusFieldSelectEl.val(),
                );
            }
        });

        let cancelButtonEl = jQuery('<a>');
        cancelButtonEl.attr('href', '#main');
        cancelButtonEl.addClass('button');
        cancelButtonEl.text('Cancel');
        controlPanelDivEl.append(cancelButtonEl);

        return petEl;
    }

    renderForm(newRecord = true) {
        let self = this;
        let petEl = jQuery('<div>');
        petEl.addClass('pet-form');

        if (newRecord) {
            let idFieldWrapEl = jQuery('<div>');
            idFieldWrapEl.addClass('field');
            petEl.append(idFieldWrapEl);

            let idFieldLabelEl = jQuery('<label>');
            idFieldLabelEl.text('ID:');
            idFieldWrapEl.append(idFieldLabelEl);

            let idFieldInputEl = jQuery('<input>');
            idFieldInputEl.addClass('field__input');
            idFieldInputEl.attr('type', 'number');
            idFieldInputEl.attr('name', 'pet-id');
            idFieldInputEl.attr('min', 1);
            idFieldInputEl.attr('value', this.#petId ? this.#petId : '');
            idFieldWrapEl.append(idFieldInputEl);
            idFieldInputEl.on('change blur keyup', function() {
                let value = jQuery(this).val();
                if (value.match(/^\d+$/) && value >= 1) {
                    if (jQuery(this).hasClass('error')) {
                        jQuery(this).removeClass('error');
                        self.#formErrors--;
                    }
                    self.#petId = value;
                } else {
                    if (!jQuery(this).hasClass('error')) {
                        self.#formErrors++;
                        jQuery(this).addClass('error');
                    }
                }
            });
        }

        let nameFieldWrapEl = jQuery('<div>');
        nameFieldWrapEl.addClass('field');
        petEl.append(nameFieldWrapEl);

        let nameFieldLabelEl = jQuery('<label>');
        nameFieldLabelEl.text('Name:');
        nameFieldWrapEl.append(nameFieldLabelEl);

        let nameFieldInputEl = jQuery('<input>');
        nameFieldInputEl.addClass('field__input');
        nameFieldInputEl.attr('type', 'text');
        nameFieldInputEl.attr('name', 'pet-name');
        nameFieldInputEl.attr('value', this.#petName);
        nameFieldWrapEl.append(nameFieldInputEl);
        nameFieldInputEl.on('change blur keyup', function () {
            let value = jQuery(this).val();
            if (value.trim() !== '') {
                if (jQuery(this).hasClass('error')) {
                    jQuery(this).removeClass('error');
                    self.#formErrors--;
                }
                self.#petName = value.trim();
            } else {
                if (!jQuery(this).hasClass('error')) {
                    self.#formErrors++;
                    jQuery(this).addClass('error');
                }
            }
        });

        let statusFieldWrapEl = jQuery('<div>');
        statusFieldWrapEl.addClass('field');
        petEl.append(statusFieldWrapEl);

        let statusFieldLabelEl = jQuery('<label>');
        statusFieldLabelEl.text('Status:');
        statusFieldWrapEl.append(statusFieldLabelEl);

        let statusFieldSelectEl = jQuery('<select>');
        statusFieldSelectEl.addClass('field__input');
        statusFieldSelectEl.attr('size', 1);
        statusFieldSelectEl.attr('name', 'pet-status');
        statusFieldWrapEl.append(statusFieldSelectEl);

        let statusOptionAvailableEl = jQuery('<option>');
        statusOptionAvailableEl.attr('value', 'available');
        if (this.#status === 'available') {
            statusOptionAvailableEl.attr('selected', 'selected');
        }
        statusOptionAvailableEl.text('available');
        statusFieldSelectEl.append(statusOptionAvailableEl);

        let statusOptionPendingEl = jQuery('<option>');
        statusOptionPendingEl.attr('value', 'pending');
        if (this.#status === 'pending') {
            statusOptionPendingEl.attr('selected', 'selected');
        }
        statusOptionPendingEl.text('pending');
        statusFieldSelectEl.append(statusOptionPendingEl);

        let statusOptionSoldEl = jQuery('<option>');
        statusOptionSoldEl.attr('value', 'sold');
        if (this.#status === 'sold') {
            statusOptionSoldEl.attr('selected', 'selected');
        }
        statusOptionSoldEl.text('sold');
        statusFieldSelectEl.append(statusOptionSoldEl);

        statusFieldSelectEl.on('change', function () {
            self.#status = jQuery(this).val();
        });

        petEl.append(this.#category.renderForm(newRecord));
        petEl.append(this.#renderTagsForm());
        petEl.append(this.#renderPicturesForm());

        let controlPanelDivEl = jQuery('<div>');
        controlPanelDivEl.addClass('controls');
        petEl.append(controlPanelDivEl);

        let saveButtonEl = jQuery('<button>');
        saveButtonEl.addClass('button');
        saveButtonEl.addClass('button--important');
        saveButtonEl.text(newRecord ? 'Create pet' : 'Update pet');
        controlPanelDivEl.append(saveButtonEl);
        saveButtonEl.on('click', function () {
            jQuery('.pet-form .field__input').change();

            if (!self.hasFormErrors()) {
                if (newRecord) {
                    self.#application.createNewPet(self);
                } else {
                    self.#application.updatePet(self);
                }
            }
        });

        let cancelButtonEl = jQuery('<a>');
        cancelButtonEl.attr('href', '#main');
        cancelButtonEl.addClass('button');
        cancelButtonEl.text('Cancel');
        controlPanelDivEl.append(cancelButtonEl);

        return petEl;
    }

    #renderTagsForm() {
        let self = this;

        let tagsFieldsetEl = jQuery('<fieldset>');
        tagsFieldsetEl.addClass('tags');

        let tagFieldsetLegendEl = jQuery('<legend>');
        tagFieldsetLegendEl.text('Tags');
        tagsFieldsetEl.append(tagFieldsetLegendEl);

        let tagsListEl = jQuery('<div>');
        tagsFieldsetEl.append(tagsListEl);

        for (let t in this.#tags) {
            tagsListEl.append(this.#tags[t].renderForm());
        }

        let controlPanelDivEl = jQuery('<div>');
        controlPanelDivEl.addClass('controls');
        tagsFieldsetEl.append(controlPanelDivEl);

        let buttonAddTagEl = jQuery('<button>');
        buttonAddTagEl.addClass('button');
        buttonAddTagEl.text('Add tag');
        controlPanelDivEl.append(buttonAddTagEl);
        buttonAddTagEl.on('click', function () {
            let newTag = new Tag(null, '');
            newTag.setPet(self);

            self.#tags.push(newTag);

            tagsListEl.append(newTag.renderForm());
        });

        return tagsFieldsetEl;
    }

    #renderPicturesForm() {
        let self = this;

        let picturesFieldsetEl = jQuery('<fieldset>');
        picturesFieldsetEl.addClass('pictures');

        let picturesFieldsetLegendEl = jQuery('<legend>');
        picturesFieldsetLegendEl.text('Pictures');
        picturesFieldsetEl.append(picturesFieldsetLegendEl);

        let picturesListEl = jQuery('<div>');
        picturesFieldsetEl.append(picturesListEl);

        for (let p in this.#pictures) {
            picturesListEl.append(this.#pictures[p].renderForm());
        }

        let controlPanelDivEl = jQuery('<div>');
        controlPanelDivEl.addClass('controls');
        picturesFieldsetEl.append(controlPanelDivEl);

        let pictureFieldWrapEl = jQuery('<div>');
        pictureFieldWrapEl.addClass('field');
        controlPanelDivEl.append(pictureFieldWrapEl);

        let pictureFieldLabelEl = jQuery('<label>');
        pictureFieldLabelEl.text('URL:');
        pictureFieldWrapEl.append(pictureFieldLabelEl);

        let pictureFieldInputEl = jQuery('<input>');
        pictureFieldInputEl.addClass('field__input');
        pictureFieldInputEl.attr('type', 'text');
        pictureFieldWrapEl.append(pictureFieldInputEl);

        let pictureFieldHintEl = jQuery('<p>');
        pictureFieldHintEl.addClass('field__hint');
        pictureFieldHintEl.text('Fill in a full URL of the picture (schema://domain.tld/path/file.ext).');
        controlPanelDivEl.append(pictureFieldHintEl);

        let buttonAddPictureEl = jQuery('<button>');
        buttonAddPictureEl.addClass('button');
        buttonAddPictureEl.text('Add picture');
        controlPanelDivEl.append(buttonAddPictureEl);
        buttonAddPictureEl.on('click', function () {
            let value = pictureFieldInputEl.val();
            if (value.match(/^[a-z]+:\/\/\w+(\.\w+)*\.\w+\/((\w?[:_\-%]?\.?)+\/)*(\w?[:_\-%]?\.?)+\.(jpg|png|gif|webp)$/)) {
                pictureFieldInputEl.removeClass('error');
            } else {
                pictureFieldInputEl.addClass('error');
                return;
            }

            let newPicture = new Picture(value);
            newPicture.setPet(self);

            picturesListEl.append(newPicture.renderForm());

            self.#pictures.push(newPicture);

            pictureFieldInputEl.val('');
        });

        return picturesFieldsetEl;
    }

    hasFormErrors() {
        if (this.#formErrors > 0) {
            return true;
        }

        if (this.#category.hasFormErrors()) {
            return true;
        }

        for (let t in this.#tags) {
            if (this.#tags[t].hasFormErrors()) {
                return true;
            }
        }

        for (let p in this.#pictures) {
            if (this.#pictures[p].hasFormErrors()) {
                return true;
            }
        }

        return false;
    }

    validateTagsIdNotContain(id, index) {
        for (let t in this.#tags) {
            if (parseInt(t) === index) {
                continue;
            }
            if (parseInt(this.#tags[t].getId()) === parseInt(id)) {
                return false;
            }
        }

        return true;
    }
}

class Filter {
    #filterBy = 'tags';
    #filterStatus = 'available';
    #filterTags = [];
    #application;
    #mainEl;

    constructor(app) {
        this.#application = app;
    }

    get by() {
        return this.#filterBy;
    }

    get status() {
        return this.#filterStatus;
    }

    get tags() {
        return this.#filterTags;
    }

    render() {
        this.#mainEl = jQuery('<div>');
        this.#mainEl.attr('id', 'filter');

        this.#renderFilterOptions();

        return this.#mainEl;
    }

    #renderFilterOptions() {
        let self = this;

        let divSelectByEl = jQuery('<div>');
        divSelectByEl.addClass('filter__type-select');
        divSelectByEl.addClass('filter__field');
        this.#mainEl.append(divSelectByEl);

        let labelBySelectEl = jQuery('<label>');
        labelBySelectEl.attr('for', 'filter-by');
        labelBySelectEl.text('Select pets by:');
        divSelectByEl.append(labelBySelectEl);

        let filterBySelectEl = jQuery('<select>');
        filterBySelectEl.attr('size', 1);
        filterBySelectEl.attr('id', 'filter-by');
        divSelectByEl.append(filterBySelectEl);

        let selectByStatusOptionEl = jQuery('<option>');
        selectByStatusOptionEl.text('status');
        selectByStatusOptionEl.attr('value', 'status');
        if (this.#filterBy === 'status') {
            selectByStatusOptionEl.attr('selected', 'selected');
        }
        filterBySelectEl.append(selectByStatusOptionEl);

        let selectByTagsOptionEl = jQuery('<option>');
        selectByTagsOptionEl.text('tags');
        selectByTagsOptionEl.attr('value', 'tags');
        if (this.#filterBy === 'tags') {
            selectByTagsOptionEl.attr('selected', 'selected');
        }
        filterBySelectEl.append(selectByTagsOptionEl);
        filterBySelectEl.on('change', function () {
            self.#switchFilterBy(jQuery(this).val());
        });

        this.#renderFilterForStatus();
        this.#renderFilterForTags();
    }

    #renderFilterForStatus() {
        if (this.#filterBy !== 'status') {
            return;
        }

        let self = this;

        let divSelectStatusEl = jQuery('<div>');
        divSelectStatusEl.addClass('filter__field');
        divSelectStatusEl.addClass('filter__status-select');
        self.#mainEl.append(divSelectStatusEl);

        let labelSelectStatusEl = jQuery('<label>');
        labelSelectStatusEl.attr('for', 'filter-status');
        labelSelectStatusEl.text('Select status:');
        divSelectStatusEl.append(labelSelectStatusEl);

        let selectStatusEl = jQuery('<select>');
        selectStatusEl.attr('size', 1);
        selectStatusEl.attr('id', 'filter-status');
        divSelectStatusEl.append(selectStatusEl);

        let selectStatusAvailableOptionEl = jQuery('<option>');
        selectStatusAvailableOptionEl.attr('value', 'available');
        selectStatusAvailableOptionEl.text('available');
        if (this.#filterStatus === 'available') {
            selectStatusAvailableOptionEl.attr('selected', 'selected');
        }
        selectStatusEl.append(selectStatusAvailableOptionEl);

        let selectStatusPendingOptionEl = jQuery('<option>');
        selectStatusPendingOptionEl.attr('value', 'pending');
        selectStatusPendingOptionEl.text('pending');
        if (this.#filterStatus === 'pending') {
            selectStatusPendingOptionEl.attr('selected', 'selected');
        }
        selectStatusEl.append(selectStatusPendingOptionEl);

        let selectStatusSoldOptionEl = jQuery('<option>');
        selectStatusSoldOptionEl.attr('value', 'sold');
        selectStatusSoldOptionEl.text('sold');
        if (this.#filterStatus === 'sold') {
            selectStatusSoldOptionEl.attr('selected', 'selected');
        }
        selectStatusEl.append(selectStatusSoldOptionEl);
        selectStatusEl.on('change', function() {
            self.#switchFilterStatus(jQuery(this).val());
        });
    }

    #renderFilterForTags() {
        if (this.#filterBy !== 'tags') {
            return;
        }

        let self = this;

        let divInputTagsEl = jQuery('<div>');
        divInputTagsEl.addClass('filter__field');
        divInputTagsEl.addClass('filter__tags');
        this.#mainEl.append(divInputTagsEl);

        let labelTagsInputEl = jQuery('<label>');
        labelTagsInputEl.attr('for', 'filter-tags');
        labelTagsInputEl.text('Fill in required tags:');
        divInputTagsEl.append(labelTagsInputEl);

        let tagsInputEl = jQuery('<input>');
        tagsInputEl.attr('type', 'text');
        tagsInputEl.attr('id', 'filter-tags');
        tagsInputEl.attr('value', this.#filterTags.join(','));
        divInputTagsEl.append(tagsInputEl);

        let tagsInputHintEl = jQuery('<p>');
        tagsInputHintEl.addClass('filter__field__hint');
        tagsInputHintEl.text('Comma separated list of tags, no spaces, letters and numbers only!');
        divInputTagsEl.append(tagsInputHintEl);

        tagsInputEl.on('keyup', function (handler) {
            if (handler.key !== 'Enter') {
                return;
            }
            if (self.#switchFilterTags(jQuery(this).val())) {
                jQuery(this).removeClass('error');
            } else {
                jQuery(this).addClass('error');
            }
        });
    }

    #switchFilterBy(to) {
        if (to !== 'status' && to !== 'tags') {
            return;
        }

        this.#filterBy = to;
        this.#application.requestPetsByFilter();
        this.#mainEl.html('');
        this.#renderFilterOptions();
    }

    #switchFilterStatus(to) {
        if (to !== 'available' && to !== 'pending' && to !== 'sold') {
            return;
        }

        this.#filterStatus = to;
        this.#application.requestPetsByFilter();
    }

    #switchFilterTags(to) {
        if (!to.match(/^([a-zA-Z0-9]+(,[a-zA-Z0-9]+)*)?$/)) {
            return false;
        }

        if (to === '') {
            this.#filterTags = [];
        } else {
            this.#filterTags = to.split(',');
        }
        this.#application.requestPetsByFilter();
        return true;
    }
}

class Application {
    #mainID;
    #asideID;
    #baseURL;
    #actualPets;
    #filter;
    #currentPath = 'main';
    #currentPet = null;

    constructor(appID, appAsideID, baseURL) {
        this.#mainID = '#' + appID;
        this.#asideID = '#' + appAsideID;
        this.#baseURL = baseURL;
        this.#actualPets = [];
        this.#filter = new Filter(this);
    }

    run() {
        this.#listenToFragmentChange();
        this.#resolvePath();
        this.#renderAll();
    }

    #listenToFragmentChange() {
        let self = this;
        window.onhashchange = function (event) {
            if (event.oldURL !== event.newURL) {
                self.#resolvePath();
                self.#renderAll(false);
            }
        }
    }

    #renderAll(withAside = true) {
        if (this.#currentPath === 'main') {
            this.requestPetsByFilter();
        } else {
            this.renderContent();
        }
        if (withAside) {
            this.#renderAside();
        }
    }

    #resolvePath() {
        let fragment = window.location.hash;

        if (fragment === '#newPet') {
            this.#currentPath = 'newPet';
            this.#currentPet = null;
            return;
        }

        let matchArray = [];

        matchArray = fragment.match(/^#editPet=(\d+)$/);
        if (matchArray !== null && matchArray[1] >= 1) {
            this.#currentPath = 'editPet';
            this.#currentPet = matchArray[1];
            return;
        }

        matchArray = fragment.match(/^#quickEditPet=(\d+)$/);
        if (matchArray !== null && matchArray[1] >= 1) {
            this.#currentPath = 'quickEditPet';
            this.#currentPet = matchArray[1];
            return;
        }

        matchArray = fragment.match(/^#uploadPicture=(\d+)$/);
        if (matchArray !== null && matchArray[1] >= 1) {
            this.#currentPath = 'uploadPicture';
            this.#currentPet = matchArray[1];
            return;
        }

        matchArray = fragment.match(/^#deletePet=(\d+)$/);
        if (matchArray !== null && matchArray[1] >= 1) {
            this.#currentPath = 'deletePet';
            this.#currentPet = matchArray[1];
            return;
        }

        this.#currentPath = 'main';
        this.#currentPet = null;
    }

    #renderAside() {
        let mainEl = jQuery(this.#asideID);
        mainEl.html('');
        mainEl.append(this.#filter.render());

        let newPetButtonWrapEl = jQuery('<div>');
        newPetButtonWrapEl.addClass('controls');
        mainEl.append(newPetButtonWrapEl);

        let newPetButtonEl = jQuery('<a>');
        newPetButtonEl.addClass('button');
        newPetButtonEl.attr('href', '#newPet');
        newPetButtonEl.text('Create new pet');
        newPetButtonWrapEl.append(newPetButtonEl);
    }

    #renderPetsList() {
        let main = jQuery(this.#mainID);
        main.html('');
        let divEl = jQuery('<div>');
        divEl.addClass('pets');
        main.append(divEl);
        for (let pet in this.#actualPets) {
            divEl.append(this.#renderPet(this.#actualPets[pet]));
        }
        if (this.#actualPets.length === 0) {
            divEl.append(this.#renderEmptyResult());
        }
    }

    #renderEmptyResult() {
        let mainDivEl = jQuery('<div>');
        mainDivEl.addClass('pet__empty-set');

        let infoEl = jQuery('<p>');
        infoEl.text('There are no pets to show according to the selected filtering criteria.');
        mainDivEl.append(infoEl);

        return mainDivEl;
    }

    #renderPet(pet) {
        return pet.render();
    }

    #renderNewPetForm() {
        let main = jQuery(this.#mainID);
        main.html('');

        let newPet = new Pet(null, '', new Category(null, ''));
        newPet.setApplication(this);
        main.append(newPet.renderForm());
    }

    #renderEditPetForm() {
        let self = this;

        let main = jQuery(this.#mainID);

        jQuery.ajax({
            url: self.#baseURL + '/pet/' + self.#currentPet,
            success: function (data) {
                let pet = self.#processPetResult(data);

                main.html('');
                main.append(pet.renderForm(false));
            },
            error: function (error) {
                if (error.status === 404) {
                    Application.showNotification('Pet not found!', 'error');
                } else {
                    Application.showNotification('Pet can\'t be loaded for unknown reason.', 'error');
                }
                location.hash = '#main';
            }
        })
    }

    #renderQuickEditForm() {
        let self = this;

        let main = jQuery(this.#mainID);

        jQuery.ajax({
            url: self.#baseURL + '/pet/' + self.#currentPet,
            success: function (data) {
                let pet = self.#processPetResult(data);

                main.html('');
                main.append(pet.renderQuickUpdateForm());
            },
            error: function (error) {
                if (error.status === 404) {
                    Application.showNotification('Pet not found!', 'error');
                } else {
                    Application.showNotification('Pet can\'t be loaded for unknown reason.', 'error');
                }
                location.hash = '#main';
            }
        })
    }

    #renderUploadPicture() {
        let self = this;

        let main = jQuery(this.#mainID);

        jQuery.ajax({
            url: self.#baseURL + '/pet/' + self.#currentPet,
            success: function (data) {
                let pet = self.#processPetResult(data);

                main.html('');
                main.append(self.#renderUploadPictureForm(pet));
            },
            error: function (error) {
                if (error.status === 404) {
                    Application.showNotification('Pet not found!', 'error');
                } else {
                    Application.showNotification('Pet can\'t be loaded for unknown reason.', 'error');
                }
                location.hash = '#main';
            }
        });
    }

    #renderUploadPictureForm(pet) {
        let all = jQuery('<div>');

        let petsEl = jQuery('<div>');
        petsEl.addClass('pets');
        all.append(petsEl);

        petsEl.append(pet.render(false));

        all.append(pet.renderUploadPictureForm());

        return all;
    }

    #renderDeletePet() {
        let self = this;

        let main = jQuery(this.#mainID);

        jQuery.ajax({
            url: self.#baseURL + '/pet/' + self.#currentPet,
            success: function (data) {
                let pet = self.#processPetResult(data);

                main.html('');
                main.append(self.#renderDeletePetForm(pet));
            },
            error: function (error) {
                if (error.status === 404) {
                    Application.showNotification('Pet not found!', 'error');
                } else {
                    Application.showNotification('Pet can\'t be loaded for unknown reason.', 'error');
                }
                location.hash = '#main';
            }
        });
    }

    #renderDeletePetForm(pet) {
        let all = jQuery('<div>');

        let petsEl = jQuery('<div>');
        petsEl.addClass('pets');
        all.append(petsEl);

        petsEl.append(pet.render(false));

        all.append(pet.renderDeletePetForm());

        return all;
    }

    renderContent() {
        if (this.#currentPath === 'main') {
            this.#renderPetsList()
        } else if (this.#currentPath === 'newPet') {
            this.#renderNewPetForm();
        } else if (this.#currentPath === 'editPet') {
            this.#renderEditPetForm();
        } else if (this.#currentPath === 'quickEditPet') {
            this.#renderQuickEditForm();
        } else if (this.#currentPath === 'uploadPicture') {
            this.#renderUploadPicture();
        } else if (this.#currentPath === 'deletePet') {
            this.#renderDeletePet();
        }
    }

    requestPetsByFilter() {
        if (this.#currentPath !== 'main') {
            return;
        }

        if (this.#filter.by === 'status') {
            this.#requestPetsByStatus();

            return;
        }

        this.#requestPetsByTags();
    }

    #processPetResult(data) {
        let category = new Category(data.category.id, data.category.name);
        let tags = [];
        let pictures = [];

        for (let t in data.tag) {
            tags.push(new Tag(data.tag[t].id, data.tag[t].name));
        }

        for (let p in data.photoUrls) {
            pictures.push(new Picture(data.photoUrls[p]));
        }

        let newPet = new Pet(
            data.id,
            data.name,
            category,
            tags,
            pictures,
            data.status,
        );
        newPet.setApplication(this);

        return newPet;
    }

    #processPetsResult(data) {
        let pets = [];

        for (let i in data) {
            pets.push(this.#processPetResult(data[i]));
        }

        this.#actualPets = pets;

        this.renderContent();
    }

    #requestPetsByStatus() {
        let self = this;
        jQuery.ajax({
            url: this.#baseURL + '/pet/findByStatus/' + this.#filter.status,
            success: function (data) {
                self.#processPetsResult(data);
            },
            error: function () {
                Application.showNotification('Can\'t load pets by status, unknown error occurred.', 'error');
            }
        });
    }

    #requestPetsByTags() {
        let self = this;
        let tags = {
            tags: []
        };
        for (let t in this.#filter.tags) {
            tags.tags.push(this.#filter.tags[t]);
        }
        let tagsQuery = jQuery.param(tags);

        jQuery.ajax({
            url: this.#baseURL + '/pet/findByTags' + (this.#filter.tags.length > 0 ? '?' + tagsQuery : ''),
            success: function (data) {
                self.#processPetsResult(data);
            },
            error: function () {
                Application.showNotification('Can\'t load pets by tags, unknown error occurred.', 'error');
            }
        });
    }

    createNewPet(pet) {
        jQuery.ajax({
            url: this.#baseURL + '/pet',
            method: 'POST',
            data: JSON.stringify(pet.json),
            contentType: 'application/json',
            success: function () {
                Application.showNotification('Pet successfully created!', 'success');
                location.hash = '#main';
            },
            error: function (error) {
                if (error.status === 409) {
                    Application.showNotification('Pet creation failed, pet already exists.', 'error');
                } else {
                    Application.showNotification('Pet creation failed, reason is unknown.', 'error');
                }
            }
        });
    }

    updatePet(pet) {
        jQuery.ajax({
            url: this.#baseURL + '/pet',
            method: 'PUT',
            data: JSON.stringify(pet.json),
            contentType: 'application/json',
            success: function () {
                Application.showNotification('Pet successfully updated!', 'success');
                location.hash = '#main';
            },
            error: function (error) {
                if (error.status === 404) {
                    Application.showNotification('Pet not found!', 'error');
                } else {
                    Application.showNotification('Pet failed to update, for unknown reason.', 'error');
                }
            }
        });
    }

    quickUpdatePet(id, name, status) {
        jQuery.ajax({
            url: this.#baseURL + '/pet/' + id + '?name=' + encodeURIComponent(name) + '&status=' + encodeURIComponent(status),
            method: 'PATCH',
            success: function () {
                Application.showNotification('Pet successfully updated!', 'success');
                location.hash = '#main';
            },
            error: function (error) {
                if (error.status === 404) {
                    Application.showNotification('Pet not found!', 'error');
                } else {
                    Application.showNotification('Pet failed to update, for unknown reason.', 'error');
                }
            }
        })
    }

    uploadPicture(id, body, type) {
        let self = this;

        jQuery.ajax({
            url: this.#baseURL + '/pet/' + id + '/uploadImage',
            method: 'POST',
            data: body,
            contentType: type,
            processData: false,
            success: function (data) {
                let pet = self.#processPetResult(data);

                let main = jQuery(self.#mainID);
                main.html('');

                main.append(self.#renderUploadPictureForm(pet));

                Application.showNotification('Picture uploaded successfully!', 'success');
            },
            error: function () {
                Application.showNotification('Failed to upload picture.', 'error');
            }
        });
    }

    deletePet(id) {
        let url = this.#baseURL + '/pet/' + id;
        console.log(url);
        jQuery.ajax({
            url: url,
            method: 'DELETE',
            success: function () {
                Application.showNotification('Pet deleted successfully!', 'success');
                location.hash = '#main';
            },
            error: function (error) {
                console.log(error);
                if (error.status === 404) {
                    Application.showNotification('Pet not found!', 'error');
                } else {
                    Application.showNotification('Pet failed to delete, for unknown reason.', 'error');
                }
            }
        });
    }

    static showNotification(text, type) {
        let notifications = jQuery('#notifications');
        if (notifications.length === 0) {
            notifications = jQuery('<div>');
            notifications.attr('id', 'notifications');
            let body = jQuery('body');
            body.append(notifications);
        }

        let notification = jQuery('<div>');
        notification.addClass('notification');
        notification.addClass('notification__' + type);
        notification.text(text);
        notifications.append(notification);
        setTimeout(function () {
            notification.fadeOut(500, function () {
                notification.remove();
            });
        }, 5000)
    }
}