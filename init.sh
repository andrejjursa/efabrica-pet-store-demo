#!/usr/bin/env bash

docker compose run --rm -w /app -e XDEBUG_START_WITH_REQUEST=no web composer install
docker compose run --rm -w /app node yarn install
docker compose run --rm -w /app node yarn run dependencies
