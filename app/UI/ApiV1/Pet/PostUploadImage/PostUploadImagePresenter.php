<?php

declare(strict_types=1);

namespace App\UI\ApiV1\Pet\PostUploadImage;

use App\Services\DataObjectMappers\Pet\PetToJsonDataMapper;
use App\Services\Pets\UpdatePetService;
use App\Services\Validators\ImageTypeValidator;
use JetBrains\PhpStorm\NoReturn;
use Nette\Application\Attributes\Requires;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Presenter;
use Nette\Http\IResponse;

#[Requires(methods: ['POST'])]
class PostUploadImagePresenter extends Presenter
{
    public function __construct(
        protected readonly ImageTypeValidator $imageTypeValidator,
        protected readonly PetToJsonDataMapper $petToJsonDataMapper,
        protected readonly UpdatePetService $updatePetService,
    ) {
        parent::__construct();
    }

    #[NoReturn]
    #[Requires(methods: ['POST'])]
    public function actionCreate(int $id): void
    {
        $httpRequest = $this->getHttpRequest();

        $body = $httpRequest->getRawBody();
        $contentType = $httpRequest->getHeader('Content-Type');

        $this->assertNonEmptyBody($body);
        $this->assertContentType($contentType);

        $this->sendJson(
            $this->petToJsonDataMapper->toJsonData(
                $this->updatePetService->uploadImageToPet($id, $body, $contentType),
            ),
        );
    }

    protected function assertNonEmptyBody(string $body): void
    {
        if (mb_strlen($body) === 0) {
            throw new BadRequestException(
                'Body must not be empty!',
                IResponse::S415_UnsupportedMediaType,
            );
        }
    }

    protected function assertContentType(string $contentType): void
    {
        if (!$this->imageTypeValidator->validate($contentType)) {
            throw new BadRequestException(
                sprintf('Unsupported image type: %s', $contentType),
                IResponse::S415_UnsupportedMediaType,
            );
        }
    }
}