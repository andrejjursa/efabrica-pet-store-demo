<?php

declare(strict_types=1);

namespace App\UI\ApiV1\Pet\GetSingle;

use App\Services\DataObjectMappers\Pet\PetToJsonDataMapper;
use App\Services\Exceptions\PetNotFoundException;
use App\Services\Pets\QueryPetService;
use JetBrains\PhpStorm\NoReturn;
use Nette\Application\Attributes\Requires;
use Nette\Application\UI\Presenter;

#[Requires(methods: ['GET'])]
class GetSinglePresenter extends Presenter
{
    public function __construct(
        protected readonly QueryPetService $queryPetService,
        protected readonly PetToJsonDataMapper $petToJsonDataMapper,
    ) {
        parent::__construct();
    }

    #[NoReturn]
    #[Requires(methods: ['GET'])]
    public function actionRead(int $id): void
    {
        $pet = $this->queryPetService->getPetById($id);

        if ($pet === null) {
            throw new PetNotFoundException(sprintf('Pet with id %s not found!', $id));
        }

        $this->sendJson($this->petToJsonDataMapper->toJsonData($pet));
    }
}