<?php

declare(strict_types=1);

namespace App\UI\ApiV1\Pet\GetByStatus;

use App\Services\DataObjectMappers\Pet\PetToJsonDataMapper;
use App\Services\DataObjects\Pet;
use App\Services\DataObjects\Status;
use App\Services\Pets\QueryPetService;
use JetBrains\PhpStorm\NoReturn;
use Nette\Application\Attributes\Requires;
use Nette\Application\UI\Presenter;

#[Requires(methods: ['GET'])]
class GetByStatusPresenter extends Presenter
{
    public function __construct(
        protected readonly QueryPetService $queryPetService,
        protected readonly PetToJsonDataMapper $petToJsonDataMapper,
    ) {
        parent::__construct();
    }

    #[NoReturn]
    #[Requires(methods: ['GET'])]
    public function actionRead(string $status): void
    {
        $this->sendJson(
            array_map(
                fn (Pet $pet) => $this->petToJsonDataMapper->toJsonData($pet),
                $this->queryPetService->getAllPetsByStatus(Status::from($status))->getResultSet(),
            ),
        );
    }
}