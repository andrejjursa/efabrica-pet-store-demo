<?php

declare(strict_types=1);

namespace App\UI\ApiV1\Pet\PostSingle;

use App\Services\DataObjectMappers\Pet\PetToJsonDataMapper;
use App\Services\Pets\CreatePetService;
use App\Services\Traits\Presenter\PresenterRestApiTrait;
use JetBrains\PhpStorm\NoReturn;
use Nette\Application\Attributes\Requires;
use Nette\Application\UI\Presenter;

#[Requires(methods: ['POST'])]
class PostSinglePresenter extends Presenter
{
    use PresenterRestApiTrait;

    public function __construct(
        protected readonly CreatePetService $createPetService,
        protected readonly PetToJsonDataMapper $petToJsonDataMapper,
    ) {
        parent::__construct();
    }

    /**
     * @throws \JsonException
     */
    #[NoReturn]
    #[Requires(methods: ['POST'])]
    public function actionCreate(): void
    {
        $httpRequest = $this->getHttpRequest();
        $this->assertRequestContentTypeIsJSON($httpRequest);

        $jsonData = json_decode($httpRequest->getRawBody(), true, flags: JSON_THROW_ON_ERROR);

        $pet = $this->createPetService->createPetFromJsonData($jsonData);

        $this->sendJson($this->petToJsonDataMapper->toJsonData($pet));
    }
}