<?php

declare(strict_types=1);

namespace App\UI\ApiV1\Pet\DeleteSingle;

use App\Services\Pets\DeletePetService;
use JetBrains\PhpStorm\NoReturn;
use Nette\Application\Attributes\Requires;
use Nette\Application\UI\Presenter;

#[Requires(methods: ['DELETE'])]
class DeleteSinglePresenter extends Presenter
{
    public function __construct(
        protected readonly DeletePetService $deletePetService,
    )
    {
        parent::__construct();
    }

    #[NoReturn]
    #[Requires(methods: ['DELETE'])]
    public function actionDelete(int $id): void
    {
        $this->deletePetService->deletePet($id);

        $this->sendJson(
            [
                'message' => 'Pet deleted successfully.',
            ],
        );
    }
}