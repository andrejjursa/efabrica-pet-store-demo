<?php

declare(strict_types=1);

namespace App\UI\ApiV1\Pet\PatchSingle;

use App\Services\DataObjectMappers\Pet\PetToJsonDataMapper;
use App\Services\DataObjects\Status;
use App\Services\Exceptions\StatusTypeError;
use App\Services\Pets\UpdatePetService;
use App\Services\Validators\StatusValueValidator;
use JetBrains\PhpStorm\NoReturn;
use Nette\Application\Attributes\Requires;
use Nette\Application\UI\Presenter;

#[Requires(methods: ['PATCH'])]
class PatchSinglePresenter extends Presenter
{
    public function __construct(
        protected readonly StatusValueValidator $statusValueValidator,
        protected readonly PetToJsonDataMapper $petToJsonDataMapper,
        protected readonly UpdatePetService $updatePetService,
    ) {
        parent::__construct();
    }


    #[NoReturn]
    #[Requires(methods: ['PATCH'])]
    public function actionUpdate(int $id, string $name, string $status): void
    {
        $this->assertValidStatus($status);

        $this->sendJson(
            $this->petToJsonDataMapper->toJsonData(
                $this->updatePetService->updatePetByNameAndStatus(
                    id: $id,
                    name: $name,
                    status: Status::from($status),
                ),
            ),
        );
    }

    protected function assertValidStatus(string $status): void
    {
        if (!$this->statusValueValidator->validate($status)) {
            throw new StatusTypeError(sprintf('Status "%s" is invalid!', $status));
        }
    }
}