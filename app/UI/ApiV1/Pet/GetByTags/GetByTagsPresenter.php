<?php

declare(strict_types=1);

namespace App\UI\ApiV1\Pet\GetByTags;

use App\Services\DataObjectMappers\Pet\PetToJsonDataMapper;
use App\Services\DataObjects\Pet;
use App\Services\Exceptions\TagTypeError;
use App\Services\Pets\QueryPetService;
use App\Services\Validators\TagValueValidator;
use JetBrains\PhpStorm\NoReturn;
use Nette\Application\Attributes\Requires;
use Nette\Application\UI\Presenter;

class GetByTagsPresenter extends Presenter
{
    public function __construct(
        protected readonly QueryPetService $queryPetService,
        protected readonly PetToJsonDataMapper $petToJsonDataMapper,
        protected readonly TagValueValidator $tagValueValidator,
    ) {
        parent::__construct();
    }


    #[NoReturn]
    #[Requires(methods: ['GET'])]
    public function actionRead(array $tags = []): void
    {
        $this->assertValidTags($tags);

        $this->sendJson(
            array_map(
                fn (Pet $pet) => $this->petToJsonDataMapper->toJsonData($pet),
                $this->queryPetService->getAllPetsByTags($tags)->getResultSet(),
            ),
        );
    }

    protected function assertValidTags(array $tags): void
    {
        foreach ($tags as $tag) {
            if (!$this->tagValueValidator->validate($tag)) {
                throw new TagTypeError(sprintf('Tag "%s" value is invalid!', $tag));
            }
        }
    }
}