<?php

declare(strict_types=1);

namespace App\UI\ApiV1\Pet\PutSingle;

use App\Services\DataObjectMappers\Pet\PetFromJsonDataMapper;
use App\Services\DataObjectMappers\Pet\PetToJsonDataMapper;
use App\Services\Pets\UpdatePetService;
use App\Services\Traits\Presenter\PresenterRestApiTrait;
use JetBrains\PhpStorm\NoReturn;
use Nette\Application\Attributes\Requires;
use Nette\Application\UI\Presenter;

#[Requires(methods: ['PUT'])]
class PutSinglePresenter extends Presenter
{
    use PresenterRestApiTrait;

    public function __construct(
        protected readonly PetFromJsonDataMapper $petFromJsonDataMapper,
        protected readonly PetToJsonDataMapper $petToJsonDataMapper,
        protected readonly UpdatePetService $updatePetService,
    ) {
        parent::__construct();
    }


    /**
     * @throws \JsonException
     */
    #[NoReturn]
    #[Requires(methods: ['PUT'])]
    public function actionUpdate(): void
    {
        $httpRequest = $this->getHttpRequest();
        $this->assertRequestContentTypeIsJSON($httpRequest);

        $jsonData = json_decode($httpRequest->getRawBody(), true, flags: JSON_THROW_ON_ERROR);

        $pet = $this->petFromJsonDataMapper->fromJsonData($jsonData);

        $this->updatePetService->updatePet($pet);

        $this->sendJson($this->petToJsonDataMapper->toJsonData($pet));
    }
}