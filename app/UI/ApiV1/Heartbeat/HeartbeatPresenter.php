<?php

namespace App\UI\ApiV1\Heartbeat;

use Nette\Application\UI\Presenter;

class HeartbeatPresenter extends Presenter
{
    public function actionRead(): void
    {
        $this->sendJson(
            [
                'time' => (new \DateTime('now'))->format('Y-m-d H:i:s'),
            ]
        );
    }
}