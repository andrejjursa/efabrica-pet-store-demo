<?php

declare(strict_types=1);

namespace App\Core;

use App\Services\DataObjects\Status;
use Contributte\ApiRouter\ApiRoute;
use Nette;
use Nette\Application\Routers\RouteList;


final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList;

        $router[] = new ApiRoute('/api/v1', 'ApiV1:Heartbeat', ['priority' => 1]);

        $router[] = new ApiRoute(
            '/api/v1/pet/<id>/uploadImage',
            'ApiV1:Pet:PostUploadImage',
            [
                'methods' => [
                    'POST' => 'create',
                ],
                'parameters' => [
                    'id' => ['requirement' => '\d+'],
                ],
                'format' => 'application/octet-stream',
            ],
        );
        $router[] = new ApiRoute(
            '/api/v1/pet/findByStatus/<status>',
            'ApiV1:Pet:GetByStatus',
            [
                'methods' => [
                    'GET' => 'read',
                ],
                'parameters' => [
                    'status' => ['requirement' => self::allowedStatusesPattern()],
                ],
                'format' => 'application/json',
            ],
        );
        $router[] = new ApiRoute(
            '/api/v1/pet/findByTags',
            'ApiV1:Pet:GetByTags',
            [
                'methods' => [
                    'GET' => 'read',
                ],
                'format' => 'application/json',
            ],
        );
        $router[] = new ApiRoute(
            '/api/v1/pet[/<id>]',
            'ApiV1:Pet:GetSingle',
            [
                'methods' => [
                    'GET' => 'read',
                ],
                'parameters' => [
                    'id' => ['requirement' => '\d+'],
                ],
                'format' => 'application/json',
            ],
        );
        $router[] = new ApiRoute(
            '/api/v1/pet[/<id>]',
            'ApiV1:Pet:DeleteSingle',
            [
                'methods' => [
                    'DELETE' => 'delete',
                ],
                'parameters' => [
                    'id' => ['requirement' => '\d+'],
                ],
                'format' => 'application/json',
            ],
        );
        $router[] = new ApiRoute(
            '/api/v1/pet[/<id>]',
            'ApiV1:Pet:PatchSingle',
            [
                'methods' => [
                    'PATCH' => 'update',
                ],
                'parameters' => [
                    'id' => ['requirement' => '\d+'],
                ],
                'format' => 'application/json',
            ],
        );
        $router[] = new ApiRoute(
            '/api/v1/pet',
            'ApiV1:Pet:PostSingle',
            [
                'methods' => [
                    'POST' => 'create',
                ],
                'format' => 'application/json',
            ],
        );
        $router[] = new ApiRoute(
            '/api/v1/pet',
            'ApiV1:Pet:PutSingle',
            [
                'methods' => [
                    'PUT' => 'update',
                ],
                'format' => 'application/json',
            ],
        );

		$router->addRoute('<presenter>/<action>[/<id>]', 'Front:Home:default');

		return $router;
	}

    protected static function allowedStatusesPattern(): string
    {
        return implode(
            '|',
            array_map(
                fn (Status $status) => $status->value,
                Status::cases(),
            ),
        );
    }
}
