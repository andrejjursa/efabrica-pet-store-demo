<?php

declare(strict_types=1);

namespace App\Services\Pets;

use App\Services\DataObjectFactories\PhotoUrlFactory;
use App\Services\DataObjects\Pet;
use App\Services\DataObjects\PhotoUrl;
use App\Services\Pets\UpdatePetService\ImageTypes;
use Ramsey\Uuid\Uuid;

class PetPicturesService
{
    public function __construct(
        protected readonly string $path,
        protected readonly string $pathPrefix,
        protected readonly PhotoUrlFactory $photoUrlFactory,
    ) {
    }

    public function writeImageToPet(Pet $pet, string $body, string $contentType): Pet
    {
        do {
            $filename = $this->getFileName($pet, $contentType);
        } while (file_exists($this->path . DIRECTORY_SEPARATOR . $filename));

        $file = fopen($this->path . DIRECTORY_SEPARATOR . $filename, "wb");
        fputs($file, $body);
        fflush($file);
        fclose($file);

        $pet->addPhotoUrl(
            $this->photoUrlFactory->createPhotoUrl(
                DIRECTORY_SEPARATOR . ltrim($this->pathPrefix, '\\/') . DIRECTORY_SEPARATOR . $filename,
            ),
        );

        return $pet;
    }

    public function deletePetImages(int $petId): void
    {
        $files = scandir($this->path);
        foreach ($files as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }
            if (!preg_match('/^' . $petId . '_/', $file)) {
                continue;
            }
            unlink($this->path . DIRECTORY_SEPARATOR . $file);
        }
    }

    public function deleteUnsetPetImages(Pet $pet): void
    {
        $pathPrefix = DIRECTORY_SEPARATOR . ltrim($this->pathPrefix, '\\/') . DIRECTORY_SEPARATOR;
        $files = scandir($this->path);
        $petImages = array_map(
            fn (string $file) => basename($file),
            array_filter(
                array_map(fn (PhotoUrl $photoUrl) => $photoUrl->getUrl(), $pet->getPhotoUrls()),
                fn (string $file) => str_starts_with($file, $pathPrefix),
            ),
        );
        foreach ($files as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }
            if (!preg_match('/^' . $pet->getId() . '_/', $file)) {
                continue;
            }
            if (in_array($file, $petImages)) {
                continue;
            }
            unlink($this->path . DIRECTORY_SEPARATOR . $file);
        }
    }

    protected function getFileName(Pet $pet, string $contentType): string
    {
        return $pet->getId() . '_' . Uuid::uuid4() . '.' . ImageTypes::getFileExtensionForType($contentType);
    }
}