<?php

declare(strict_types=1);

namespace App\Services\Pets;

use App\Services\DataObjectMappers\Pet\PetFromJsonDataMapper;
use App\Services\DataObjectMappers\Pet\PetToXMLDataMapper;
use App\Services\DataObjects\Pet;
use App\Services\Exceptions\PetCreationException;
use App\Services\FluidXmlFactory;
use App\Services\Pets\QueryPetService\HydratorType;
use FluidXml\FluidXml;
use Nette\Http\IResponse;

class CreatePetService
{
    public function __construct(
        protected readonly PetFromJsonDataMapper $petFromJsonDataMapper,
        protected readonly FluidXmlFactory $fluidXmlFactory,
        protected readonly QueryPetService $queryPetService,
        protected readonly PetDatabaseService $petDatabaseService,
        protected readonly PetToXMLDataMapper $petToXMLDataMapper,
    ) {
    }

    public function createPetFromJsonData(array $jsonData): Pet
    {
        $pet = $this->petFromJsonDataMapper->fromJsonData($jsonData);

        return $this->createPet($pet);
    }

    public function createPet(Pet $pet): Pet
    {
        try {
            $this->petDatabaseService->openDatabaseForWrite();

            $existingPet = $this->queryPetService->getPetById($pet->getId());

            if ($existingPet !== null) {
                throw new PetCreationException('Pet already exists.', IResponse::S409_Conflict);
            }

            $this->addPet($pet);

            return $pet;
        } finally {
            $this->petDatabaseService->closePetDatabase();
        }
    }

    protected function addPet(Pet $pet): void
    {
        try {
            $this->petDatabaseService->openDatabaseForWrite();

            $allPetsXml = $this->queryPetService->getAllPets(HydratorType::FLUID_XML)->getResultSet();
            assert($allPetsXml instanceof FluidXml);
            $allPetsXml->add($this->petToXMLDataMapper->toXMLData($pet));

            $this->petDatabaseService->writeDatabaseXML($allPetsXml->xml());
        } finally {
            $this->petDatabaseService->closePetDatabase();
        }
    }
}