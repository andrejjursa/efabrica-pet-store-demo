<?php

namespace App\Services\Pets;

use App\Services\DataObjectMappers\Pet\PetFromDOMElementMapper;
use App\Services\DataObjects\Pet;
use App\Services\DataObjects\Status;
use App\Services\FluidXmlFactory;
use App\Services\Pets\QueryPetService\HydratorFactory;
use App\Services\Pets\QueryPetService\HydratorType;
use App\Services\Pets\QueryPetService\ResultSetInterface;

class QueryPetService
{
    public function __construct(
        protected readonly FluidXmlFactory $fluidXmlFactory,
        protected readonly PetDatabaseService $petDatabaseService,
        protected readonly PetFromDOMElementMapper $petFromDOMElementMapper,
        protected readonly HydratorFactory $hydratorFactory,
    ) {
    }

    public function getPetById(int $id): ?Pet
    {
        $pets = $this->getAllByQuery('pet[id="' . $id . '"]');

        if (count($pets->getResultSet()) !== 1) {
            return null;
        }

        return $pets->getResultSet()[0];
    }

    public function getAllPets(HydratorType $type = HydratorType::LIST): ResultSetInterface
    {
        return $this->getAllByQuery('pet', $type);
    }

    public function getAllPetsByStatus(Status $status, HydratorType $type = HydratorType::LIST): ResultSetInterface
    {
        return $this->getAllByQuery(sprintf('./pet[./status = \'%s\']', $status->value), $type);
    }

    public function getAllPetsByTags(array $tags, HydratorType $type = HydratorType::LIST): ResultSetInterface
    {
        if (count($tags) === 0) {
            return $this->getAllPets();
        }

        $query[] = './pet[';

        foreach ($tags as $tag) {
            $query[] = sprintf('./tags/tag = \'%s\'', $tag);
        }

        $query[] = ']';

        $query = $query[0] .
            implode(' or ', array_slice($query, 1, count($query) - 2)) .
            $query[count($query) - 1];

        return $this->getAllByQuery($query, $type);
    }

    protected function getAllByQuery(string $query, HydratorType $type = HydratorType::LIST): ResultSetInterface
    {
        try {
            $this->petDatabaseService->openDatabaseForRead();

            $database = $this->petDatabaseService->readPetDatabase();

            $fluidXml = $this->fluidXmlFactory->build($database);

            $petsXML = $fluidXml->query($query);

            $hydrator = $this->hydratorFactory->createHydrator($type);

            return $hydrator->hydrate($petsXML);
        } finally {
            $this->petDatabaseService->closePetDatabase();
        }
    }
}