<?php

declare(strict_types=1);

namespace App\Services\Pets;

use App\Services\DataObjectMappers\Pet\PetToXMLDataMapper;
use App\Services\DataObjects\Pet;
use App\Services\DataObjects\Status;
use App\Services\Exceptions\PetNotFoundException;
use App\Services\FluidXmlFactory;
use App\Services\Pets\QueryPetService\HydratorType;
use App\Services\Traits\Service\Pets\DatabaseReconstructionTrait;

class UpdatePetService
{
    use DatabaseReconstructionTrait;

    public function __construct(
        protected readonly QueryPetService $queryPetService,
        protected readonly PetDatabaseService $petDatabaseService,
        protected readonly FluidXmlFactory $fluidXmlFactory,
        protected readonly PetToXMLDataMapper $petToXMLDataMapper,
        protected readonly PetPicturesService $petPicturesService,
    ) {
    }

    public function updatePet(Pet $pet): void
    {
        try {
            $this->petDatabaseService->openDatabaseForWrite();

            if ($this->queryPetService->getPetById($pet->getId()) === null) {
                throw new PetNotFoundException(sprintf('Pet with id %s not found!', $pet->getId()));
            }

            $this->updateSinglePet($pet);
        } finally {
            $this->petDatabaseService->closePetDatabase();
        }
    }

    public function updatePetByNameAndStatus(int $id, string $name, Status $status): Pet
    {
        try {
            $this->petDatabaseService->openDatabaseForWrite();

            $pet = $this->queryPetService->getPetById($id);

            if ($pet === null) {
                throw new PetNotFoundException(sprintf('Pet with id %s not found!', $id));
            }

            $pet->setName($name);
            $pet->setStatus($status);

            return $this->updateSinglePet($pet);
        } finally {
            $this->petDatabaseService->closePetDatabase();
        }
    }

    public function uploadImageToPet(int $id, string $body, string $contentType): Pet
    {
        try {
            $this->petDatabaseService->openDatabaseForWrite();

            $pet = $this->queryPetService->getPetById($id);

            if ($pet === null) {
                throw new PetNotFoundException(sprintf('Pet with id %s not found!', $id));
            }

            return $this->updateSinglePet(
                $this->petPicturesService->writeImageToPet($pet, $body, $contentType),
            );
        } finally {
            $this->petDatabaseService->closePetDatabase();
        }
    }

    public function updateSinglePet(Pet $pet): Pet
    {
        $pets = $this->queryPetService->getAllPets(HydratorType::INDEXED_ARRAY)->getResultSet();
        $pets[$pet->getId()] = $pet;

        $databaseXml = $this->reconstructDatabase($pets);

        $this->petDatabaseService->writeDatabaseXML($databaseXml->xml());

        $this->petPicturesService->deleteUnsetPetImages($pet);

        return $pet;
    }
}