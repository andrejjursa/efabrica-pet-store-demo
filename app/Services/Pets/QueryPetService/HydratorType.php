<?php

declare(strict_types=1);

namespace App\Services\Pets\QueryPetService;

enum HydratorType
{
    case LIST;
    case FLUID_XML;
    case INDEXED_ARRAY;
}
