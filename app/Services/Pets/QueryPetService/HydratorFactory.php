<?php

declare(strict_types=1);

namespace App\Services\Pets\QueryPetService;

use App\Services\DataObjectMappers\Pet\PetFromDOMElementMapper;
use App\Services\Pets\QueryPetService\Hydrator\IndexedArrayHydrator;
use App\Services\Pets\QueryPetService\Hydrator\ListHydrator;
use App\Services\Pets\QueryPetService\Hydrator\NullHydrator;
use App\Services\Pets\QueryPetService\Hydrator\XMLHydrator;

class HydratorFactory
{
    public function __construct(
        protected readonly PetFromDOMElementMapper $petFromDOMElementMapper,
    ) {
    }

    public function createHydrator(HydratorType $type): HydratorInterface
    {
        return $this->buildListHydrator($type)
            ?? $this->buildXMLHydrator($type)
            ?? $this->buildIndexedArrayHydrator($type)
            ?? new NullHydrator();
    }

    protected function buildListHydrator(hydratorType $type): ?ListHydrator
    {
        if ($type === HydratorType::LIST) {
            return new ListHydrator($this->petFromDOMElementMapper);
        }

        return null;
    }

    protected function buildXMLHydrator(HydratorType $type): ?XMLHydrator
    {
        if ($type === HydratorType::FLUID_XML) {
            return new XMLHydrator();
        }

        return null;
    }

    protected function buildIndexedArrayHydrator(HydratorType $type): ?IndexedArrayHydrator
    {
        if ($type === HydratorType::INDEXED_ARRAY) {
            return new IndexedArrayHydrator($this->petFromDOMElementMapper);
        }

        return null;
    }
}