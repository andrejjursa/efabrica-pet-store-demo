<?php

declare(strict_types=1);

namespace App\Services\Pets\QueryPetService;

interface ResultSetInterface
{
    public function getResultSet(): mixed;
}