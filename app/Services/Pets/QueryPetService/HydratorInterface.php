<?php

declare(strict_types=1);

namespace App\Services\Pets\QueryPetService;

use FluidXml\FluidContext;

interface HydratorInterface
{
    public function hydrate(FluidContext $xmlData): ResultSetInterface;
}