<?php

declare(strict_types=1);

namespace App\Services\Pets\QueryPetService\ResultSet;

use App\Services\Pets\QueryPetService\ResultSetInterface;
use FluidXml\FluidXml;

class XMLResultSet implements ResultSetInterface
{
    public function __construct(protected FluidXml $xml)
    {
    }

    public function getResultSet(): FluidXml
    {
        return $this->xml;
    }
}