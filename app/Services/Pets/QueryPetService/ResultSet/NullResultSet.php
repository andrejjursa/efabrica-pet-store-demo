<?php

declare(strict_types=1);

namespace App\Services\Pets\QueryPetService\ResultSet;

use App\Services\Pets\QueryPetService\ResultSetInterface;

class NullResultSet implements ResultSetInterface
{
    public function getResultSet(): null
    {
        return null;
    }
}