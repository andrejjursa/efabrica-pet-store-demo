<?php

declare(strict_types=1);

namespace App\Services\Pets\QueryPetService\ResultSet;

use App\Services\DataObjects\Pet;
use App\Services\Pets\QueryPetService\ResultSetInterface;

class ListResultSet implements ResultSetInterface
{
    /**
     * @param array<Pet> $results
     */
    public function __construct(protected readonly array $results)
    {
    }

    /**
     * @return array<Pet>
     */
    public function getResultSet(): array
    {
        return $this->results;
    }
}