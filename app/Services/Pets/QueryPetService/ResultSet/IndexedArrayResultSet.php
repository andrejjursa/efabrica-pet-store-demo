<?php

declare(strict_types=1);

namespace App\Services\Pets\QueryPetService\ResultSet;

use App\Services\DataObjects\Pet;
use App\Services\Pets\QueryPetService\ResultSetInterface;

class IndexedArrayResultSet implements ResultSetInterface
{
    /**
     * @param array<int, Pet> $results
     */
    public function __construct(protected readonly array $results)
    {
    }


    /**
     * @return array<int, Pet>
     */
    public function getResultSet(): array
    {
        return $this->results;
    }
}