<?php

declare(strict_types=1);

namespace App\Services\Pets\QueryPetService\Hydrator;

use App\Services\Pets\QueryPetService\HydratorInterface;
use App\Services\Pets\QueryPetService\ResultSet\NullResultSet;
use FluidXml\FluidContext;

class NullHydrator implements HydratorInterface
{
    public function hydrate(FluidContext $xmlData): NullResultSet
    {
        return new NullResultSet();
    }
}