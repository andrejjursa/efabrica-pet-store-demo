<?php

declare(strict_types=1);

namespace App\Services\Pets\QueryPetService\Hydrator;

use App\Services\FluidXmlOverride;
use App\Services\Pets\QueryPetService\HydratorInterface;
use App\Services\Pets\QueryPetService\ResultSet\XMLResultSet;
use FluidXml\FluidContext;

class XMLHydrator implements HydratorInterface
{
    public function hydrate(FluidContext $xmlData): XMLResultSet
    {
        $fluidXml = new FluidXmlOverride('pets');
        $fluidXml->add($xmlData);

        return new XMLResultSet($fluidXml);
    }
}