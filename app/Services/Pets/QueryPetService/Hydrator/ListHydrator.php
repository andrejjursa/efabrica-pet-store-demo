<?php

declare(strict_types=1);

namespace App\Services\Pets\QueryPetService\Hydrator;

use App\Services\DataObjectMappers\Pet\PetFromDOMElementMapper;
use App\Services\Pets\QueryPetService\HydratorInterface;
use App\Services\Pets\QueryPetService\ResultSet\ListResultSet;
use FluidXml\FluidContext;

class ListHydrator implements HydratorInterface
{
    public function __construct(
        protected readonly PetFromDOMElementMapper $petFromDOMElementMapper,
    ) {
    }

    public function hydrate(FluidContext $xmlData): ListResultSet
    {
        $pets = [];

        foreach ($xmlData as $petXML) {
            $pets[] = $this->petFromDOMElementMapper->fromDOMElement($petXML);
        }

        return new ListResultSet($pets);
    }
}