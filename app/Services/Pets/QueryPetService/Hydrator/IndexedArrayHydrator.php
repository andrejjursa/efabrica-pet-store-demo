<?php

namespace App\Services\Pets\QueryPetService\Hydrator;

use App\Services\DataObjectMappers\Pet\PetFromDOMElementMapper;
use App\Services\Pets\QueryPetService\HydratorInterface;
use App\Services\Pets\QueryPetService\ResultSet\IndexedArrayResultSet;
use FluidXml\FluidContext;

class IndexedArrayHydrator implements HydratorInterface
{
    public function __construct(
        protected readonly PetFromDOMElementMapper $petFromDOMElementMapper,
    ) {
    }

    public function hydrate(FluidContext $xmlData): IndexedArrayResultSet
    {
        $pets = [];

        foreach ($xmlData as $petXML) {
            $pet = $this->petFromDOMElementMapper->fromDOMElement($petXML);
            $pets[$pet->getId()] = $pet;
        }

        return new IndexedArrayResultSet($pets);
    }
}