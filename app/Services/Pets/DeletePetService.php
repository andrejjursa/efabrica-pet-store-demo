<?php

declare(strict_types=1);

namespace App\Services\Pets;

use App\Services\Exceptions\PetNotFoundException;
use App\Services\Pets\QueryPetService\HydratorType;

class DeletePetService
{
    public function __construct(
        protected readonly QueryPetService $queryPetService,
        protected readonly PetDatabaseService $petDatabaseService,
        protected readonly PetPicturesService $petPicturesService,
    ) {
    }

    public function deletePet(int $id): void
    {
        try {
            $this->petDatabaseService->openDatabaseForWrite();

            $existingPet = $this->queryPetService->getPetById($id);

            if ($existingPet === null) {
                throw new PetNotFoundException(sprintf('Pet with id %d not found.', $id));
            }

            $allPetsXML = $this->queryPetService->getAllPets(HydratorType::FLUID_XML)->getResultSet();
            $allPetsXML->remove(sprintf('./pet[@id = \'%d\']', $id));

            $this->petDatabaseService->writeDatabaseXML($allPetsXML->xml());

            $this->petPicturesService->deletePetImages($id);
        } finally {
            $this->petDatabaseService->closePetDatabase();
        }
    }
}