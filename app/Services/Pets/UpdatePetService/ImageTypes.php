<?php

declare(strict_types=1);

namespace App\Services\Pets\UpdatePetService;

class ImageTypes
{
    public static array $imageTypes = [
        'image/jpeg',
        'image/gif',
        'image/png',
        'image/webp',
    ];

    public static function getFileExtensionForType(string $type): string
    {
        return match ($type) {
            'image/png' => 'png',
            'image/jpeg' => 'jpg',
            'image/gif' => 'gif',
            'image/webp' => 'webp',
            default => 'tmp',
        };
    }
}