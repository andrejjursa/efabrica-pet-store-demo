<?php

declare(strict_types=1);

namespace App\Services\Pets;

use App\Services\Exceptions\DatabaseReadException;

class PetDatabaseService
{
    /** @var null|resource */
    protected static $filePointer = null;

    protected static int $instances = 0;

    protected static int $filePointerOpened = 0;

    public function __construct(
        protected readonly string $databaseFilePath,
    ) {
        self::$instances++;
    }

    public function __destruct()
    {
        self::$instances--;

        if (self::$instances > 0) {
            return;
        }

        try {
            while (true) {
                $this->assertDatabaseOpen();

                $this->closePetDatabase();
            }
        } catch (DatabaseReadException) {
            // Database already closed ...
        }
    }

    public function readPetDatabase(): string
    {
        $this->assertDatabaseOpen();

        if (flock(self::$filePointer, LOCK_SH)) {
            fseek(self::$filePointer, 0);
            return fread(self::$filePointer, filesize($this->databaseFilePath));
        }

        throw new DatabaseReadException('Failed to lock database!');
    }

    public function openDatabaseForRead(): void
    {
        self::$filePointerOpened++;

        if (is_resource(self::$filePointer)) {
            return;
        }

        $this->reCreateDatabase();

        self::$filePointer = fopen($this->databaseFilePath, 'r+');

        if (!flock(self::$filePointer, LOCK_SH)) {
            throw new DatabaseReadException('Failed to lock database!');
        }
    }

    public function openDatabaseForWrite(): void
    {
        self::$filePointerOpened++;

        if (is_resource(self::$filePointer)) {
            return;
        }

        $this->reCreateDatabase();

        self::$filePointer = fopen($this->databaseFilePath, 'r+');

        if (!flock(self::$filePointer, LOCK_EX)) {
            throw new DatabaseReadException('Failed to lock database!');
        }
    }

    public function writeDatabaseXML(string $xml): void
    {
        $this->assertDatabaseOpen();

        if (flock(self::$filePointer, LOCK_EX | LOCK_NB)) {
            ftruncate(self::$filePointer, 0);
            fflush(self::$filePointer);
            fseek(self::$filePointer, 0);
            fwrite(self::$filePointer, $xml);
            fflush(self::$filePointer);

            return;
        }

        throw new DatabaseReadException('Failed to lock database!');
    }

    public function closePetDatabase(): void
    {
        if (!is_resource(self::$filePointer) || self::$filePointerOpened === 0) {
            return;
        }

        self::$filePointerOpened--;

        if (self::$filePointerOpened === 0) {
            fflush(self::$filePointer);
            flock(self::$filePointer, LOCK_UN);
            fclose(self::$filePointer);
            self::$filePointer = null;
        }
    }

    protected function assertDatabaseNotOpen(): void
    {
        if (self::$filePointer !== null && is_resource(self::$filePointer)) {
            throw new DatabaseReadException('Database already opened!');
        }
    }

    protected function assertDatabaseOpen(): void
    {
        if (self::$filePointer === null || !is_resource(self::$filePointer)) {
            throw new DatabaseReadException('Database is not opened!');
        }
    }

    protected function reCreateDatabase(): void
    {
        if (file_exists($this->databaseFilePath)) {
            return;
        }

        $this->assertDatabaseNotOpen();

        self::$filePointer = fopen($this->databaseFilePath, 'w+');

        if (flock(self::$filePointer, LOCK_EX)) {
            ftruncate(self::$filePointer, 0);
            fwrite(
                self::$filePointer,
                '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL .
                '<pets>' . PHP_EOL .
                '</pets>',
            );
            fflush(self::$filePointer);
            flock(self::$filePointer, LOCK_UN);
            fclose(self::$filePointer);

            return;
        }

        throw new DatabaseReadException('Database cannot be re-created!');
    }
}