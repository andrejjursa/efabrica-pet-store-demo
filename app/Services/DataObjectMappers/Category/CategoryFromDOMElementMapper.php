<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Category;

use App\Services\DataObjectFactories\CategoryFactory;
use App\Services\DataObjects\Category;
use App\Services\Exceptions\CategoryTypeError;
use DOMElement;

class CategoryFromDOMElementMapper
{
    public function __construct(
        protected CategoryFactory $categoryFactory,
    ) {
    }

    public function fromDOMElement(DOMElement $DOMElement): Category
    {
        $this->assertHasValidId($DOMElement);
        $this->assertCategoryIsValid($DOMElement);

        $id = (int)$DOMElement->getAttribute('id');
        $name = $DOMElement->textContent;

        return $this->categoryFactory->createCategory($id, $name);
    }

    protected function assertHasValidId(DOMElement $DOMElement): void
    {
        if (!$DOMElement->hasAttribute("id")) {
            throw new CategoryTypeError('Supplied DOM element is not a Category: missing id parameter!');
        }

        if (!is_numeric($DOMElement->getAttribute("id"))) {
            throw new CategoryTypeError('Category id is not numeric!');
        }
    }

    protected function assertCategoryIsValid(DOMElement $DOMElement): void
    {
        $children = $DOMElement->childNodes->length;

        if ($children === 0) {
            return;
        }

        if ($children > 1) {
            throw new CategoryTypeError(
                'Supplied DOM element is not a Category: node must have zero or one child!',
            );
        }

        if (!$DOMElement->childNodes->item(0) instanceof \DOMText) {
            throw new CategoryTypeError('Supplied DOM element is not a Category: node is not a text!');
        }
    }
}