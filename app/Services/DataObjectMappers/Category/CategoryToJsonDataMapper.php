<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Category;

use App\Services\DataObjects\Category;

class CategoryToJsonDataMapper
{
    public function toJsonData(Category $category): array
    {
        return [
            'id' => $category->getId(),
            'name' => $category->getName(),
        ];
    }
}