<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Category;

use App\Services\DataObjectFactories\CategoryFactory;
use App\Services\DataObjects\Category;
use App\Services\Exceptions\CategoryTypeError;

class CategoryFromJsonDataMapper
{
    public function __construct(protected CategoryFactory $categoryFactory)
    {
    }

    public function fromJsonData(array $data): Category
    {
        $this->assertValidId($data);
        $this->assertValidName($data);

        return $this->categoryFactory->createCategory($data['id'], $data['name']);
    }

    protected function assertValidId(array $data): void
    {
        if (!array_key_exists("id", $data)) {
            throw new CategoryTypeError('Missing id parameter!');
        }

        if (!is_int($data["id"])) {
            throw new CategoryTypeError('Category id must be an integer!');
        }
    }

    protected function assertValidName(array $data): void
    {
        if (!array_key_exists("name", $data)) {
            throw new CategoryTypeError('Missing name parameter!');
        }

        if (!is_string($data["name"])) {
            throw new CategoryTypeError('Category name must be a string!');
        }
    }
}