<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Category;

use App\Services\DataObjects\Category;

class CategoryToXMLDataMapper
{
    public function toXMLData(Category $category): string
    {
        return sprintf('<category id="%d">%s</category>', $category->getId(), $category->getName());
    }
}