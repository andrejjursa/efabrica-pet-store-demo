<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Pet;

use App\Services\DataObjectMappers\Category\CategoryToJsonDataMapper;
use App\Services\DataObjectMappers\PhotoUrl\PhotoUrlToJsonDataMapper;
use App\Services\DataObjectMappers\Status\StatusToJsonDataMapper;
use App\Services\DataObjectMappers\Tag\TagToJsonDataMapper;
use App\Services\DataObjects\Pet;
use App\Services\DataObjects\PhotoUrl;
use App\Services\DataObjects\Tag;

class PetToJsonDataMapper
{
    public function __construct(
        protected CategoryToJsonDataMapper $categoryToJsonDataMapper,
        protected TagToJsonDataMapper $tagToJsonDataMapper,
        protected StatusToJsonDataMapper  $statusToJsonDataMapper,
        protected PhotoUrlToJsonDataMapper $photoUrlToJsonDataMapper,
    ) {
    }

    public function toJsonData(Pet $pet): array
    {
        return [
            'id' => $pet->getId(),
            'name' => $pet->getName(),
            'category' => $this->categoryToJsonDataMapper->toJsonData($pet->getCategory()),
            'photoUrls' => array_map(
                fn(PhotoUrl $photoUrl) => $this->photoUrlToJsonDataMapper->toJsonData($photoUrl),
                $pet->getPhotoUrls(),
            ),
            'tag' => array_map(fn(Tag $tag) => $this->tagToJsonDataMapper->toJsonData($tag), $pet->getTags()),
            'status' => $this->statusToJsonDataMapper->toJsonData($pet->getStatus()),
        ];
    }
}