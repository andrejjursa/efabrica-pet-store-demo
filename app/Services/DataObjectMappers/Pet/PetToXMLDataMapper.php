<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Pet;

use App\Services\DataObjectMappers\Category\CategoryToXMLDataMapper;
use App\Services\DataObjectMappers\PhotoUrl\PhotoUrlToXMLDataMapper;
use App\Services\DataObjectMappers\Status\StatusToXMLDataMapper;
use App\Services\DataObjectMappers\Tag\TagToXMLDataMapper;
use App\Services\DataObjects\Pet;
use App\Services\DataObjects\PhotoUrl;
use App\Services\DataObjects\Tag;

class PetToXMLDataMapper
{
    public function __construct(
        protected CategoryToXMLDataMapper $categoryToXMLDataMapper,
        protected PhotoUrlToXMLDataMapper $photoUrlToXMLDataMapper,
        protected StatusToXMLDataMapper $statusToXMLDataMapper,
        protected TagToXMLDataMapper $tagToXMLDataMapper,
    ) {
    }

    public function toXMLData(Pet $pet): string
    {
        $structure = <<<XML
<pet id="%d">
    <name>%s</name>
    %s
    <photoUrls>
        %s
    </photoUrls>
    <tags>
        %s
    </tags>
    %s
</pet>
XML;

        return sprintf(
            $structure,
            $pet->getId(),
            $pet->getName(),
            $this->categoryToXMLDataMapper->toXMLData($pet->getCategory()),
            implode("\n", array_map(
                fn(PhotoUrl $photoUrl) => $this->photoUrlToXMLDataMapper->toXMLData($photoUrl),
                $pet->getPhotoUrls(),
            )),
            implode("\n", array_map(
                fn(Tag $tag) => $this->tagToXMLDataMapper->toXMLData($tag),
                $pet->getTags(),
            )),
            $this->statusToXMLDataMapper->toXMLData($pet->getStatus()),
        );
    }
}