<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Pet;

use App\Services\DataObjectFactories\PetFactory;
use App\Services\DataObjectMappers\Category\CategoryFromJsonDataMapper;
use App\Services\DataObjectMappers\PhotoUrl\PhotoUrlFromJsonDataMapper;
use App\Services\DataObjectMappers\Status\StatusFromJsonDataMapper;
use App\Services\DataObjectMappers\Tag\TagFromJsonDataMapper;
use App\Services\DataObjects\Pet;
use App\Services\Exceptions\PetTypeError;

class PetFromJsonDataMapper
{
    public function __construct(
        protected CategoryFromJsonDataMapper $categoryFromJsonDataMapper,
        protected PhotoUrlFromJsonDataMapper $photoUrlFromJsonDataMapper,
        protected StatusFromJsonDataMapper $statusFromJsonDataMapper,
        protected TagFromJsonDataMapper $tagFromJsonDataMapper,
        protected PetFactory $petFactory,
    ) {
    }

    public function fromJsonData(array $data): Pet
    {
        $this->assertValidId($data);
        $this->assertValidName($data);
        $this->assertValidCategory($data);
        $this->assertValidPhotoUrls($data);
        $this->assertValidTags($data);
        $this->assertValidId($data);

        return $this->petFactory->createPet(
            id: $data['id'],
            name: $data['name'],
            category: $this->categoryFromJsonDataMapper->fromJsonData($data['category']),
            photoUrls: array_map(
                fn (string $photoUrl) => $this->photoUrlFromJsonDataMapper->fromJsonData($photoUrl),
                $data['photoUrls'],
            ),
            tags: array_map(
                fn (array $tag) => $this->tagFromJsonDataMapper->fromJsonData($tag),
                $data['tags'],
            ),
            status: $this->statusFromJsonDataMapper->fromJsonData($data['status']),
        );
    }

    protected function assertValidId(array $data): void
    {
        if (!array_key_exists("id", $data)) {
            throw new PetTypeError('Missing id parameter!');
        }

        if (!is_int($data["id"])) {
            throw new PetTypeError('Pet id must be an integer!');
        }
    }

    protected function assertValidName(array $data): void
    {
        if (!array_key_exists("name", $data)) {
            throw new PetTypeError('Missing name parameter!');
        }

        if (!is_string($data["name"])) {
            throw new PetTypeError('Pet name must be a string!');
        }
    }

    protected function assertValidCategory(array $data): void
    {
        if (!array_key_exists("category", $data)) {
            throw new PetTypeError('Missing category parameter!');
        }

        if (!is_array($data["category"])) {
            throw new PetTypeError('Pet category must be an array!');
        }
    }

    protected function assertValidPhotoUrls(array $data): void
    {
        if (!array_key_exists("photoUrls", $data)) {
            throw new PetTypeError('Missing photoUrls parameter!');
        }

        if (!is_array($data["photoUrls"])) {
            throw new PetTypeError('Pet photoUrls must be an array!');
        }

        if (count($data["photoUrls"]) === 0) {
            return;
        }

        foreach ($data["photoUrls"] as $photoUrl) {
            if (!is_string($photoUrl)) {
                throw new PetTypeError('Each pet photoUrl entity must be a string!');
            }
        }
    }

    protected function assertValidTags(array $data): void
    {
        if (!array_key_exists("tags", $data)) {
            throw new PetTypeError('Missing tags parameter!');
        }

        if (!is_array($data["tags"])) {
            throw new PetTypeError('Pet tags must be an array!');
        }
    }

    protected function assertValidStatus(array $data): void
    {
        if (!array_key_exists("status", $data)) {
            throw new PetTypeError('Missing status parameter!');
        }

        if (!is_string($data["status"])) {
            throw new PetTypeError('Pet status must be a string!');
        }
    }
}