<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Pet;

use App\Services\DataObjectFactories\PetFactory;
use App\Services\DataObjectMappers\Category\CategoryFromDOMElementMapper;
use App\Services\DataObjectMappers\PhotoUrl\PhotoUrlFromDOMElementMapper;
use App\Services\DataObjectMappers\Status\StatusFromDOMElementMapper;
use App\Services\DataObjectMappers\Tag\TagFromDOMElementMapper;
use App\Services\DataObjects\Pet;
use App\Services\Exceptions\PetTypeError;
use App\Services\Traits\Mapper\DOMElementChildFinderTrait;
use DOMElement;
use DOMText;

class PetFromDOMElementMapper
{
    use DOMElementChildFinderTrait;

    public function __construct(
        protected PetFactory $petFactory,
        protected CategoryFromDOMElementMapper $categoryFromDOMElementMapper,
        protected PhotoUrlFromDOMElementMapper $photoUrlFromDOMElementMapper,
        protected TagFromDOMElementMapper $tagFromDOMElementMapper,
        protected StatusFromDOMElementMapper $statusFromDOMElementMapper,
    ) {
    }

    public function fromDOMElement(DOMElement $DOMElement): Pet
    {
        $this->assertHasValidId($DOMElement);
        $this->assertHasName($DOMElement);
        $this->assertHasCategory($DOMElement);
        $this->assertHasPhotoUrls($DOMElement);
        $this->assertHasTags($DOMElement);
        $this->assertHasStatus($DOMElement);
        $this->assertPhotoUrlsChildrenAreValid($DOMElement);
        $this->assertTagsChildrenAreValid($DOMElement);

        $id = (int)$DOMElement->getAttribute('id');
        $nameElement = $this->getNamedChildElement($DOMElement, 'name');

        $this->assertNameElementIsValid($nameElement);

        $name = $nameElement->textContent;

        $photoUrlsElement = $this->getNamedChildElement($DOMElement, 'photoUrls');

        $photoUrls = [];

        foreach ($photoUrlsElement->childNodes as $photoUrlElement) {
            if (!$photoUrlElement instanceof DOMElement) {
                continue;
            }
            $photoUrls[] = $this->photoUrlFromDOMElementMapper->fromDOMElement($photoUrlElement);
        }

        $tagsElement = $this->getNamedChildElement($DOMElement, 'tags');

        $tags = [];

        foreach ($tagsElement->childNodes as $tagElement) {
            if (!$tagElement instanceof DOMElement) {
                continue;
            }
            $tags[] = $this->tagFromDOMElementMapper->fromDOMElement($tagElement);
        }

        return $this->petFactory->createPet(
            id: $id,
            name: $name,
            category: $this->categoryFromDOMElementMapper->fromDOMElement(
                $this->getNamedChildElement($DOMElement, 'category'),
            ),
            photoUrls: $photoUrls,
            tags: $tags,
            status: $this->statusFromDOMElementMapper->fromDOMElement(
                $this->getNamedChildElement($DOMElement, 'status'),
            ),
        );
    }

    protected function assertHasValidId(DOMElement $DOMElement): void
    {
        if (!$DOMElement->hasAttribute("id")) {
            throw new PetTypeError('Supplied DOM element is not a Pet: missing id parameter!');
        }

        if (!is_numeric($DOMElement->getAttribute("id"))) {
            throw new PetTypeError('Pet id is not numeric!');
        }
    }

    protected function assertHasName(DOMElement $DOMElement): void
    {
        $child = $this->getNamedChildElement($DOMElement, 'name');

        if ($child === null) {
            throw new PetTypeError('Supplied DOM element is not a Pet: missing name child element!');
        }
    }

    protected function assertHasCategory(DOMElement $DOMElement): void
    {
        $child = $this->getNamedChildElement($DOMElement, 'category');

        if ($child === null) {
            throw new PetTypeError('Supplied DOM element is not a Pet: missing category child element!');
        }
    }

    protected function assertHasPhotoUrls(DOMElement $DOMElement): void
    {
        $child = $this->getNamedChildElement($DOMElement, 'photoUrls');

        if ($child === null) {
            throw new PetTypeError('Supplied DOM element is not a Pet: missing photoUrls child element!');
        }
    }

    protected function assertHasTags(DOMElement $DOMElement): void
    {
        $child = $this->getNamedChildElement($DOMElement, 'tags');

        if ($child === null) {
            throw new PetTypeError('Supplied DOM element is not a Pet: missing tags child element!');
        }
    }

    protected function assertHasStatus(DOMElement $DOMElement): void
    {
        $child = $this->getNamedChildElement($DOMElement, 'status');

        if ($child === null) {
            throw new PetTypeError('Supplied DOM element is not a Pet: missing status child element!');
        }
    }

    protected function assertNameElementIsValid(DOMElement $DOMElement): void
    {
        $children = $DOMElement->childNodes->length;

        if ($children === 0) {
            return;
        }

        if ($children > 1) {
            throw new PetTypeError('Supplied DOM element is not a Pet: name node must have zero or one child!');
        }

        if (!$DOMElement->childNodes->item(0) instanceof DOMText) {
            throw new PetTypeError('Supplied DOM element is not a Pet: name node is not a text!');
        }
    }

    protected function assertPhotoUrlsChildrenAreValid(DOMElement $DOMElement): void
    {
        $children = $this->getNamedChildElement($DOMElement, 'photoUrls')->childNodes;

        if ($children->length === 0) {
            return;
        }

        if ($children->length === 1 && $children->item(0) instanceof DOMText) {
            return;
        }

        foreach ($children as $child) {
            assert($child instanceof DOMElement);

            if ($child->nodeName !== 'photoUrl') {
                throw new PetTypeError('Supplied Photo URLs child is not a photoUrl node!');
            }
        }
    }

    protected function assertTagsChildrenAreValid(DOMElement $DOMElement): void
    {
        $children = $this->getNamedChildElement($DOMElement, 'tags')->childNodes;

        if ($children->length === 0) {
            return;
        }

        if ($children->length === 1 && $children->item(0) instanceof DOMText) {
            return;
        }

        foreach ($children as $child) {
            assert($child instanceof DOMElement);

            if ($child->nodeName !== 'tag') {
                throw new PetTypeError('Supplied tags child is not a tag node!');
            }
        }
    }
}