<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Status;

use App\Services\DataObjects\Status;

class StatusToJsonDataMapper
{
    public function toJsonData(Status $status): string
    {
        return $status->value;
    }
}