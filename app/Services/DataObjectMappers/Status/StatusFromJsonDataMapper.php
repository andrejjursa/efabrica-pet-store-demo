<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Status;

use App\Services\DataObjects\Status;
use App\Services\Exceptions\StatusTypeError;

class StatusFromJsonDataMapper
{
    public function fromJsonData(string $data): Status
    {
        $this->assertValidStatus($data);

        return Status::from($data);
    }

    protected function assertValidStatus(string $data): void
    {
        $allowedValues = array_map(fn (\UnitEnum $case) => $case->value, Status::cases());

        if (!in_array($data, $allowedValues, true)) {
            throw new StatusTypeError('Invalid status!');
        }
    }
}