<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Status;

use App\Services\DataObjects\Status;
use App\Services\Exceptions\StatusTypeError;
use DOMElement;

class StatusFromDOMElementMapper
{
    public function fromDOMElement(DOMElement $DOMElement): Status
    {
        $this->assertIsValid($DOMElement);

        return Status::from($DOMElement->textContent);
    }

    public function assertIsValid(DOMElement $DOMElement): void
    {
        $children = $DOMElement->childNodes->length;

        if ($children !== 1) {
            throw new StatusTypeError(
                'Supplied DOM element is not a Status: node must contain one text child!',
            );
        }

        if (!$DOMElement->childNodes->item(0) instanceof \DOMText) {
            throw new StatusTypeError('Supplied DOM element is not a Status: node is not a text!');
        }

        if ($DOMElement->childNodes->item(0)->textContent === '') {
            throw new StatusTypeError('Supplied DOM element is not a Status: text node is empty!');
        }

        $allowedValues = array_map(fn (\UnitEnum $case) => $case->value, Status::cases());

        if (!in_array($DOMElement->childNodes->item(0)->textContent, $allowedValues, true)) {
            throw new StatusTypeError('Supplied DOM element is not a Status: unrecognized value!');
        }
    }
}