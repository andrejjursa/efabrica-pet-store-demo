<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Status;

use App\Services\DataObjects\Status;

class StatusToXMLDataMapper
{
    public function toXMLData(Status $status): string
    {
        return '<status>' . $status->value . '</status>';
    }
}