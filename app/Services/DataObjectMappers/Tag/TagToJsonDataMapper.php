<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Tag;

use App\Services\DataObjects\Tag;

class TagToJsonDataMapper
{
    public function toJsonData(Tag $tag): array
    {
        return [
            'id' => $tag->getId(),
            'name' => $tag->getName(),
        ];
    }
}