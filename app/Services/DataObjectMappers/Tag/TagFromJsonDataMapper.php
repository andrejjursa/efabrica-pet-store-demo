<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Tag;

use App\Services\DataObjectFactories\TagFactory;
use App\Services\DataObjects\Tag;
use App\Services\Exceptions\TagTypeError;
use App\Services\Validators\TagValueValidator;

class TagFromJsonDataMapper
{
    public function __construct(
        protected TagFactory $tagFactory,
        protected TagValueValidator $tagValueValidator,
    ) {
    }

    public function fromJsonData(array $data): Tag
    {
        $this->assertValidId($data);
        $this->assertValidName($data);

        return $this->tagFactory->createTag($data['id'], $data['name']);
    }

    protected function assertValidId(array $data): void
    {
        if (!array_key_exists('id', $data)) {
            throw new TagTypeError('Missing id parameter!');
        }

        if (!is_int($data['id'])) {
            throw new TagTypeError('Tag id must be an integer!');
        }
    }

    protected function assertValidName(array $data): void
    {
        if (!array_key_exists('name', $data)) {
            throw new TagTypeError('Missing name parameter!');
        }

        if (!is_string($data['name'])) {
            throw new TagTypeError('Tag name must be a string!');
        }

        if (!$this->tagValueValidator->validate($data['name'])) {
            throw new TagTypeError('Invalid tag value!');
        }
    }
}