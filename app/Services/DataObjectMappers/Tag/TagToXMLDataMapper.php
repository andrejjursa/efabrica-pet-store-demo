<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Tag;

use App\Services\DataObjects\Tag;

class TagToXMLDataMapper
{
    public function toXMLData(Tag $tag): string
    {
        return sprintf('<tag id="%d">%s</tag>', $tag->getId(), $tag->getName());
    }
}