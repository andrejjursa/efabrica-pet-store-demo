<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\Tag;

use App\Services\DataObjectFactories\TagFactory;
use App\Services\DataObjects\Tag;
use App\Services\Exceptions\TagTypeError;
use App\Services\Validators\TagValueValidator;
use DOMElement;

class TagFromDOMElementMapper
{
    public function __construct(
        protected TagFactory $tagFactory,
        protected TagValueValidator $tagValueValidator,
    ) {
    }

    public function fromDOMElement(DOMElement $DOMElement): Tag
    {
        $this->assertHasValidId($DOMElement);
        $this->assertTagIsValid($DOMElement);

        $id = (int)$DOMElement->getAttribute('id');
        $name = $DOMElement->textContent;

        return $this->tagFactory->createTag($id, $name);
    }

    protected function assertHasValidId(DOMElement $DOMElement): void
    {
        if (!$DOMElement->hasAttribute("id")) {
            throw new TagTypeError('Supplied DOM element is not a Tag: missing id parameter!');
        }

        if (!is_numeric($DOMElement->getAttribute("id"))) {
            throw new TagTypeError('Tag id is not numeric!');
        }
    }

    protected function assertTagIsValid(DOMElement $DOMElement): void
    {
        $children = $DOMElement->childNodes->length;

        if ($children === 0) {
            return;
        }

        if ($children > 1) {
            throw new TagTypeError(
                'Supplied DOM element is not a Tag: node must have zero or one child!',
            );
        }

        if (!$DOMElement->childNodes->item(0) instanceof \DOMText) {
            throw new TagTypeError('Supplied DOM element is not a Tag: node is not a text!');
        }

        if (!$this->tagValueValidator->validate($DOMElement->childNodes->item(0)->textContent)) {
            throw new TagTypeError('Supplied DOM element is not a Tag: invalid tag value!');
        }
    }
}