<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\PhotoUrl;

use App\Services\DataObjectFactories\PhotoUrlFactory;
use App\Services\DataObjects\PhotoUrl;
use App\Services\Exceptions\PhotoUrlTypeError;
use DOMElement;

class PhotoUrlFromDOMElementMapper
{
    public function __construct(protected PhotoUrlFactory $photoUrlFactory)
    {
    }

    public function fromDOMElement(DOMElement $DOMElement): PhotoUrl
    {
        $this->assertIsValid($DOMElement);

        $url = $DOMElement->textContent;

        return $this->photoUrlFactory->createPhotoUrl($url);
    }

    public function assertIsValid(DOMElement $DOMElement): void
    {
        $children = $DOMElement->childNodes->length;

        if ($children !== 1) {
            throw new PhotoUrlTypeError(
                'Supplied DOM element is not a Photo URL: node must contain one text child!',
            );
        }

        if (!$DOMElement->childNodes->item(0) instanceof \DOMText) {
            throw new PhotoUrlTypeError('Supplied DOM element is not a Photo URL: node is not a text!');
        }

        if ($DOMElement->childNodes->item(0)->textContent === '') {
            throw new PhotoUrlTypeError('Supplied DOM element is not a Photo URL: text node is empty!');
        }
    }
}