<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\PhotoUrl;

use App\Services\DataObjectFactories\PhotoUrlFactory;
use App\Services\DataObjects\PhotoUrl;
use App\Services\Exceptions\PhotoUrlTypeError;

class PhotoUrlFromJsonDataMapper
{
    public function __construct(protected PhotoUrlFactory $photoUrlFactory)
    {
    }

    public function fromJsonData(string $data): PhotoUrl
    {
        $this->assertValidUrl($data);

        return $this->photoUrlFactory->createPhotoUrl($data);
    }

    protected function assertValidUrl(string $data): void
    {
        if (strlen($data) === 0) {
            throw new PhotoUrlTypeError('Photo URL must not be empty!');
        }
    }
}