<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\PhotoUrl;

use App\Services\DataObjects\PhotoUrl;

class PhotoUrlToJsonDataMapper
{
    public function toJsonData(PhotoUrl $photoUrl): string
    {
        return $photoUrl->getUrl();
    }
}