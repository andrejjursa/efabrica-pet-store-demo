<?php

declare(strict_types=1);

namespace App\Services\DataObjectMappers\PhotoUrl;

use App\Services\DataObjects\PhotoUrl;

class PhotoUrlToXMLDataMapper
{
    public function toXMLData(PhotoUrl $photoUrl): string
    {
        return '<photoUrl>' . $photoUrl->getUrl() . '</photoUrl>';
    }
}