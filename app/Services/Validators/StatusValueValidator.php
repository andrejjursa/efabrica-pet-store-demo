<?php

declare(strict_types=1);

namespace App\Services\Validators;

use App\Services\DataObjects\Status;

class StatusValueValidator
{
    public static function validate(string $value): bool
    {
        $cases = array_map(fn (Status $status) => $status->value, Status::cases());

        return in_array($value, $cases, true);
    }
}