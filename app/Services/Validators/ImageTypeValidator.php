<?php

declare(strict_types=1);

namespace App\Services\Validators;

use App\Services\Pets\UpdatePetService\ImageTypes;

class ImageTypeValidator
{
    public function validate(string $imageType): bool
    {
        return in_array($imageType, ImageTypes::$imageTypes, true);
    }
}