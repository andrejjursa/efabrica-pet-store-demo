<?php

declare(strict_types=1);

namespace App\Services\Validators;

class TagValueValidator
{
    public function validate(string $tagValue): bool
    {
        return (bool)preg_match('/^[a-zA-Z0-9]+[a-zA-Z0-9_-]*$/', $tagValue);
    }
}