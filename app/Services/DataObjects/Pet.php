<?php

declare(strict_types=1);

namespace App\Services\DataObjects;

class Pet
{
    public function __construct(
        protected readonly int $id,
        protected string $name,
        protected Category $category,
        /** @var array<PhotoUrl> */
        protected array $photoUrls = [],
        /** @var array<Tag> */
        protected array $tags = [],
        protected Status $status = Status::AVAILABLE,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Pet
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): Pet
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return array<PhotoUrl>
     */
    public function getPhotoUrls(): array
    {
        return $this->photoUrls;
    }

    public function addPhotoUrl(PhotoUrl $url): Pet
    {
        $this->photoUrls[] = $url;

        return $this;
    }

    public function removePhotoUrl(PhotoUrl $photoUrl): Pet
    {
        $this->photoUrls = array_filter($this->photoUrls, fn ($url) => $url->getUrl() !== $photoUrl->getUrl());

        return $this;
    }

    /**
     * @param array<PhotoUrl> $photoUrls
     */
    public function setPhotoUrls(array $photoUrls): Pet
    {
        $this->photoUrls = $photoUrls;

        return $this;
    }

    /**
     * @return array<Tag>
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): Pet
    {
        $this->tags[] = $tag;

        return $this;
    }

    public function removeTag(Tag $tag): Pet
    {
        $this->tags = array_filter(
            $this->tags,
            function (Tag $storedTag) use ($tag) {
                return $storedTag->getId() !== $tag->getId();
            }
        );

        return $this;
    }

    /**
     * @param array<Tag> $tags
     */
    public function setTags(array $tags): Pet
    {
        $this->tags = $tags;

        return $this;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): Pet
    {
        $this->status = $status;

        return $this;
    }
}