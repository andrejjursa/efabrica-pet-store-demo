<?php

declare(strict_types=1);

namespace App\Services\DataObjects;

enum Status: string
{
    case AVAILABLE = 'available';
    case PENDING = 'pending';
    case SOLD = 'sold';
}
