<?php

declare(strict_types=1);

namespace App\Services\DataObjects;

class PhotoUrl
{
    public function __construct(
        protected readonly string $url,
    ){
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}