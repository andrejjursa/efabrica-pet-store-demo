<?php

declare(strict_types=1);

namespace App\Services\DataObjectFactories;

use App\Services\DataObjects\Tag;

class TagFactory
{
    public function createTag(int $id, string $name): Tag
    {
        return new Tag($id, $name);
    }
}