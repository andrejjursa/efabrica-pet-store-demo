<?php

declare(strict_types=1);

namespace App\Services\DataObjectFactories;

use App\Services\DataObjects\Category;
use App\Services\DataObjects\Pet;
use App\Services\DataObjects\PhotoUrl;
use App\Services\DataObjects\Status;
use App\Services\DataObjects\Tag;
use App\Services\Exceptions\PhotoUrlTypeError;
use App\Services\Exceptions\TagTypeError;

class PetFactory
{
    /**
     * @param array<PhotoUrl> $photoUrls
     * @param array<Tag> $tags
     * @throws PhotoUrlTypeError|TagTypeError
     */
    public function createPet(
        int $id,
        string $name,
        Category $category,
        array $photoUrls = [],
        array $tags = [],
        Status $status = Status::AVAILABLE,
    ): Pet
    {
        $this->assertPhotoUrls($photoUrls);
        $this->assertTags($tags);

        return new Pet($id, $name, $category, $photoUrls, $tags, $status);
    }

    /**
     * @param array<PhotoUrl> $photoUrls
     * @throws PhotoUrlTypeError
     */
    protected function assertPhotoUrls(array $photoUrls): void
    {
        foreach ($photoUrls as $photoUrl) {
            if (!$photoUrl instanceof PhotoUrl) {
                throw new PhotoUrlTypeError('Each photo URL must be an instance of ' . PhotoUrl::class . '!');
            }
        }
    }

    /**
     * @param array<Tag> $tags
     * @throws TagTypeError
     */
    protected function assertTags(array $tags): void
    {
        foreach ($tags as $tag) {
            if (!$tag instanceof Tag) {
                throw new TagTypeError('Each tag must be an instance of ' . Tag::class . '!');
            }
        }
    }
}