<?php

declare(strict_types=1);

namespace App\Services\DataObjectFactories;

use App\Services\DataObjects\Category;

class CategoryFactory
{
    public function createCategory(int $id, string $name): Category
    {
        return new Category($id, $name);
    }
}