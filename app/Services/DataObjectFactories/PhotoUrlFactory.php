<?php

declare(strict_types=1);

namespace App\Services\DataObjectFactories;

use App\Services\DataObjects\PhotoUrl;

class PhotoUrlFactory
{
    public function createPhotoUrl(string $url): PhotoUrl
    {
        return new PhotoUrl($url);
    }
}