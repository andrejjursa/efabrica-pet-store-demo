<?php

namespace App\Services\Exceptions;

use Nette\Application\BadRequestException;
use Nette\Http\IResponse;
use Throwable;

class PetNotFoundException extends BadRequestException
{
    public function __construct(string $message = "", Throwable $previous = null)
    {
        parent::__construct($message, IResponse::S404_NotFound, $previous);
    }
}