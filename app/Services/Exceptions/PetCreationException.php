<?php

namespace App\Services\Exceptions;

use Nette\Application\BadRequestException;

class PetCreationException extends BadRequestException
{

}