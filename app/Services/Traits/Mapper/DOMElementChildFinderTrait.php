<?php

declare(strict_types=1);

namespace App\Services\Traits\Mapper;

use DOMElement;

trait DOMElementChildFinderTrait
{
    protected function getNamedChildElement(DOMElement $DOMElement, string $childNodeName): ?DOMElement
    {
        foreach ($DOMElement->childNodes as $DOMElementChild) {
            if ($DOMElementChild->nodeName === $childNodeName) {
                return $DOMElementChild;
            }
        }

        return null;
    }
}