<?php

namespace App\Services\Traits\Presenter;

use Nette\Application\BadRequestException;
use Nette\Http\IResponse;
use Nette\Http\Request;

trait PresenterRestApiTrait
{
    /**
     * @throws BadRequestException
     */
    protected function assertRequestContentTypeIsJSON(Request $request): void
    {
        $contentType = $request->getHeader('Content-Type');
        if ($contentType === 'application/json') {
            return;
        }

        throw new BadRequestException(
            'Only application/json requests are accepted!',
            IResponse::S400_BadRequest,
        );
    }
}