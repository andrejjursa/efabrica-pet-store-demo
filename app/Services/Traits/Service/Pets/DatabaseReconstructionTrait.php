<?php

declare(strict_types=1);

namespace App\Services\Traits\Service\Pets;

use App\Services\DataObjectMappers\Pet\PetToXMLDataMapper;
use App\Services\FluidXmlFactory;
use FluidXml\FluidXml;

trait DatabaseReconstructionTrait
{
    protected readonly FluidXmlFactory $fluidXmlFactory;
    protected readonly PetToXMLDataMapper $petToXMLDataMapper;

    protected function reconstructDatabase(array $pets): FluidXml
    {
        $fluidXml = $this->fluidXmlFactory->build('pets');

        foreach ($pets as $pet) {
            $fluidXml->add($this->petToXMLDataMapper->toXMLData($pet));
        }

        return $fluidXml;
    }
}