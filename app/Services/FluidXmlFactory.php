<?php

declare(strict_types=1);

namespace App\Services;

use FluidXml\FluidXml;

class FluidXmlFactory
{
    public function build(?string $xml = null): FluidXml
    {
        return new FluidXmlOverride($xml);
    }
}